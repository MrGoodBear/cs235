/***********************************************************************
 * Header:
 *    NODE
 * Summary:
 *    a node class!
 * Author
 *    Cameron Lewis
 ************************************************************************/

#ifndef WEEK07_NODE_H
#define WEEK07_NODE_H

template <class T>
class Node {
 public:
  Node() : data(0), pNext(0), pPrev(0){};
  Node(T newData) {
    data = newData;
    pNext = 0;
    pPrev = 0;
  }

  T data;
  Node<T>* pNext;
  Node<T>* pPrev;
};

#endif  // WEEK07_NODE_H
