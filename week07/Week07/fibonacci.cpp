/***********************************************************************
 * Implementation:
 *    FIBONACCI
 * Summary:
 *    This will contain the implementation for fibonacci() as well as any
 *    other function or class implementations you may need
 * Author
 *   Cameron Lewis
 **********************************************************************/

#include <iostream>
#include "fibonacci.h"  // for fibonacci() prototype
#include "list.h"       // for LIST
using namespace std;

/************************************************
 * FIBONACCI
 * The function allowing the user to
 * display Fibonacci numbers
 ***********************************************/
void fibonacci() {
  // show the first few Fibonacci numbers
  int number;
  List<int> sequence;
  bool bigFibonacci = false;
  bigNumber single_Fibonacci;

  cout << "How many Fibonacci numbers would you like to see? ";
  cin >> number;

  calculateFibonacci(number, sequence, bigFibonacci, single_Fibonacci);
  displayFibonacci(sequence);

  // prompt for a single large Fibonacci
  cout << "Which Fibonacci number would you like to display? ";
  cin >> number;

  bigFibonacci = true;
  calculateFibonacci(number, sequence, bigFibonacci, single_Fibonacci);
  single_Fibonacci.display();
}

/************************************************
 * calculateFibonacci
 * Calculates the number in the fibonacci sequence
 ***********************************************/
void calculateFibonacci(int number,
                        List<int>& sequence,
                        bool size,
                        bigNumber& single_Fibonacci) {
  if (number <= 0)
    return;

  if (!size) {
    int a = 1;
    int b = 1;
    sequence.push_back(a);
    sequence.push_back(b);
    // cout << "\t" << a << endl << "\t" << b << endl;
    for (int i = 2; i < number; i++) {
      // Compute the next pair of fibonacci numbers.
      int oldA = a;
      a = b;
      b += oldA;
      sequence.push_back(b);
    }  // for end
    return;
  }  // if end

  else {
    bigNumber a;
    a.insert(1);
    single_Fibonacci.insert(1);

    for (int i = 2; i < number; i++) {
      bigNumber temp = a;
      a = single_Fibonacci;
      single_Fibonacci += temp;
    }
    return;
  }  // else end
}

/************************************************
 * DISPLAYFIBONACCI
 * Displays the fibonacci number in the sequence
 ***********************************************/
void displayFibonacci(List<int> sequence) {
  for (ListIterator<int> it = sequence.begin(); it != sequence.end(); ++it)
    cout << "\t" << *it << endl;
}  //*/
