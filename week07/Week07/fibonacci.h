/***********************************************************************
 * Header:
 *    FIBONACCI
 * Summary:
 *    This will contain just the prototype for fibonacci(). You may
 *    want to put other class definitions here as well.
 * Author
 *    Cameron Lewis
 ************************************************************************/

#include "list.h"
#include <iostream>
using namespace std;

#ifndef FIBONACCI_H
#define FIBONACCI_H

class bigNumber {
 public:
  bigNumber() {}
  ~bigNumber() {}
  bigNumber(unsigned int number) {}
  bigNumber(const bigNumber& rhs) { this->data = rhs.data; }

  // inserts a number into the front
  void insert(int number) { data.push_front(number); }

  bigNumber& operator=(bigNumber rhs) {
    this->data = rhs.data;
    return *this;
  }

  bigNumber& operator+=(bigNumber& rhs) {
    ListIterator<int> itRHS = rhs.data.rbegin();
    ListIterator<int> it = data.rbegin();
    int first = 0;
    int second = 0;
    List<int> sum;
    int result = 0;
    int carry = 0;

    while (itRHS != rhs.data.rend() || it != data.rend()) {
      // set first
      if (itRHS != rhs.data.rend()) {
        first = *itRHS;
        --itRHS;

      } else
        first = 0;

      // set second
      if (it != rhs.data.rend()) {
        second = *it;
        --it;

      } else
        second = 0;

      int temp = first + second + carry;
      result = temp % 1000;
      carry = temp / 1000;
      sum.push_front(result);
    }

    if (carry > 0)
      sum.push_front(carry);

    data = sum;
    return *this;
  }

  // displays the list
  void display() {
    cout << "\t";
    for (ListIterator<int> it = this->data.begin(); it != this->data.end();
         ++it) {
      if (*it < 10 && it != data.begin())
        cout << "00" << *it;
      else if (*it < 100 && it != data.begin())
        cout << '0' << *it;
      else
        cout << *it;

      if (it.p->pNext != NULL)
        cout << ",";
    }
    cout << endl;
  }

 private:
  List<int> data;
};

// the interactive fibonacci program
void fibonacci();

// calculates the fibonacci number
void calculateFibonacci(int number,
                        List<int>& sequence,
                        bool size,
                        bigNumber& single_Fibanacci);

// displays the fibonacci sequence
void displayFibonacci(List<int> sequence);

#endif  // FIBONACCI_H
