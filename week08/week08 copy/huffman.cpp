/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Cameron Lewis

 ************************************************************************/

#include "huffman.h"  // for HUFFMAN() prototype

using namespace std;

/*******************************************
 * HUFFMAN
 * Driver program to exercise the huffman generation code
 *******************************************/

void huffman(string fileName) {
  List<Pair<string, string> >* codeData = new List<Pair<string, string> >();
  // creates huffmanTree object
  Huffman<double, string>* huffmanTree = new Huffman<double, string>();
  // Build out the binaryTree for use in creating huffman code
  huffmanTree->buildTree(huffmanTree->data);

  //  makes a new list
  huffmanTree->data = new List<BinaryNode<Pair<double, string> > >();

  huffmanTree->sortCode(codeData);

  huffmanTree->loadData(huffmanTree->data, fileName);
  // print out the results
  huffmanTree->printCode(codeData);

  // get the codes
  huffmanTree->getCode(&huffmanTree->data->front(), "", codeData);
};
