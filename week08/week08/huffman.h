/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother JonesL, CS 235
 * Author:
 *    Cameron Lewis
 ************************************************************************/

#include "list.h"
#include "bnode.h"
#include "pair.h"
#include <iostream>  // for ISTREAM and OSTREAM
#include <string>    // for STRING
#include <cassert>   // for ASSERT
#include <fstream>
using namespace std;

#ifndef HUFFMAN_H
#define HUFFMAN_H

// driver program
void huffman(string fileName);

/*******************************************
 * HUFFMAN
 * Class for creating a huffman code from file
 *******************************************/

template <class T1, class T2>
class Huffman {
 public:
  // default constructor
  Huffman() throw(const char*) {
    try {
      data = NULL;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  };

  //  sort list
  void sortCode(List<Pair<string, string> >* codeData);

  //  load data from file into List
  void loadData(List<BinaryNode<Pair<T1, T2> > >* fileData, string fileName);

  void getCode(BinaryNode<Pair<T1, T2> >* root,
               string str,
               List<Pair<T2, T2> >* codeData);

  List<BinaryNode<Pair<T1, T2> > >* data;

  //  Read list data
  void buildTree(List<BinaryNode<Pair<T1, T2> > >* huffmanTree);

  //  Display sorted list
  void printCode(List<Pair<T2, T2> >* rhs);

  ListIterator<BinaryNode<Pair<T1, T2> > > smallestValue(
      List<BinaryNode<Pair<T1, T2> > >* fileData);
};

/*******************************************
 * loadData
 * Function for getting data loaded from file into a list.
 *******************************************/
template <class T1, class T2>
void Huffman<T1, T2>::loadData(List<BinaryNode<Pair<T1, T2> > >* fileData,
                               string fileName) {
  try {
    string dataValue;
    double frequency;

    ifstream fsFileData(fileName.c_str());

    while (fsFileData >> dataValue >> frequency) {
      fileData->push_back(
          BinaryNode<Pair<T1, T2> >(Pair<T1, T2>(frequency, dataValue)));
    }
  } catch (exception ex) {
    cout << "Exception encountered";
  }
};
/*******************************************
 * getCode
 * Fuction that gets 1 or 0 value used for
 * Huffman codes
 *******************************************/
template <class T1, class T2>
void Huffman<T1, T2>::getCode(BinaryNode<Pair<T1, T2> >* root,
                              string str,
                              List<Pair<T2, T2> >* codeData) {
  if (!root)
    return;

  if (root->data.second != "")
    codeData->push_back(Pair<string, string>(root->data.second, str));

  getCode(root->pLeft, str + "0", codeData);
  getCode(root->pRight, str + "1", codeData);
};
/*******************************************
 * buildTree
 *******************************************/
template <class T1, class T2>
void Huffman<T1, T2>::buildTree(List<BinaryNode<Pair<T1, T2> > >* huffmanTree) {
  while (huffmanTree->size() != 1) {
    ListIterator<BinaryNode<Pair<T1, T2> > > it = smallestValue(huffmanTree);
    BinaryNode<Pair<T1, T2> >* left = new BinaryNode<Pair<T1, T2> >(it.p->data);

    BinaryNode<Pair<T1, T2> >* newTemp = new BinaryNode<Pair<T1, T2> >();
    huffmanTree->insert(it, *newTemp);

    ListIterator<BinaryNode<Pair<T1, T2> > > removeIt =
        ListIterator<BinaryNode<Pair<T1, T2> > >(it.p->pNext);
    huffmanTree->remove(removeIt);

    // get the smallest frequency still in the BinaryTree
    ListIterator<BinaryNode<Pair<T1, T2> > > it2 = smallestValue(huffmanTree);
    BinaryNode<Pair<T1, T2> >* right =
        new BinaryNode<Pair<T1, T2> >(it2.p->data);

    huffmanTree->remove(it2);

    left->pParent = &it.operator*();
    right->pParent = &it.operator*();

    // add left and rights frequency to create the frequency for the new node
    it.p->data.data.first = (T1)(left->data.first + right->data.first);

    it.p->data.addLeft(left);
    it.p->data.addRight(right);
  }
}

/*******************************************
 * sortCode
 * Function for sorting the List of Code pair
 * values
 *******************************************/
template <class T1, class T2>
void Huffman<T1, T2>::sortCode(List<Pair<string, string> >* codeData) {
  for (ListIterator<Pair<string, string> > j = codeData->begin();
       j != codeData->end(); ++j) {
    for (ListIterator<Pair<string, string> > k = j; k != codeData->end(); ++k) {
      if (j.operator*().first > k.operator*().first) {
        Pair<string, string> temp = *j;
        *j = *k;
        *k = temp;
      }
    }
  }
};

/*******************************************
 * printCode
 * prints the code pair values from Huffman codes
 *******************************************/
template <class T1, class T2>
void Huffman<T1, T2>::printCode(List<Pair<T2, T2> >* rhs) {
  ListIterator<Pair<T2, T2> > it;
  for (it = rhs->begin(); it != rhs->end(); ++it)
    cout << it.operator*().first << " = " << it.operator*().second << endl;
};

/*******************************************
 * smallestValue
 * Finds the smallest value which recurs the
 * least.
 *******************************************/
template <class T1, class T2>
ListIterator<BinaryNode<Pair<T1, T2> > > Huffman<T1, T2>::smallestValue(
    List<BinaryNode<Pair<T1, T2> > >* fileData) {
  double smallestValue = -1.0;
  ListIterator<BinaryNode<Pair<T1, T2> > > iteratorSmallest;
  for (ListIterator<BinaryNode<Pair<T1, T2> > > j = fileData->begin();
       j != fileData->end(); ++j) {
    if ((j.operator*().data.first < smallestValue || smallestValue < 0.0) &&
        j.operator*().data.first != 0) {
      smallestValue = j.operator*().data.first;
      iteratorSmallest = j;
    }
  }
  return iteratorSmallest;
};

#endif  // HUFFMAN_h
