/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Cameron Lewis
 ************************************************************************/

#include "huffman.h"       // for HUFFMAN() prototype

using namespace std;


/*******************************************
 * HUFFMAN
 *******************************************/

void huffman(string fileName)
{
    Huffman< double , string > * huffmanTree = new Huffman< double , string >();
    huffmanTree->data =  new List < BinaryNode < Pair < double , string > > >();
    huffmanTree->loadData(huffmanTree->data,fileName);
    huffmanTree->buildTree(huffmanTree->data);
    List < Pair < string , string > > * codeData = new List < Pair < string , string > > ();
    
    huffmanTree->getCode(&huffmanTree->data->front() , "", codeData);
    huffmanTree->sortCode(codeData);
    huffmanTree->printCode(codeData);
};


