#ifndef Deque_h
#define Deque_h
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <new>
#include <stdexcept>
#include <string>
using namespace std;

/************************************************
 * TEMPLATE CLASS: DEQUE
 * An array-based container class which has data
 * accessible from both the front and back.
 ***********************************************/
template <typename T> class Deque {
public:
    // default constructor : empty and kinda useless
    Deque() : maxItems(0), numItems(0),  myFront(0), myBack(0) {
        allocate(maxItems);
    };

    // copy constructor : copy it
    Deque(const Deque &rhs) throw(const char *);

    // non-default constructor : pre-allocate
    Deque(int maxItems) throw(const char *);

    // destructor : free everything
    ~Deque() {
        if (maxItems)
            data = NULL;
    }

    // pushes stuff to the front of the deque
    void push_front(const T &t) throw(const char *);

    // pushes stuff to the back of the deque
    void push_back(const T &t) throw(const char *);

    // is it empty?
    bool empty() const;

    // removes stuff from the front of the deque
    void pop_front() throw(const char *);

    // removes stuff from the back of the deque
    void pop_back() throw(const char *);

    // returns data at the front of deque
    const T &front() throw(const char *);

    // returns data at the back of deque
    const T &back() throw(const char *);

    // remove all the items from the container
    void clear() {
        numItems = 0;
    }

    // how many items are currently in the container?
    int size() const {
        return numItems;
    }

    // returns the current capacity of the Deque
    int capacity() const {
        return maxItems;
    }

    // overloaded assignment operator
    Deque<T> & operator = (const Deque<T> &rhs)
    {
        if (maxItems != rhs.maxItems) {
            allocate(rhs.maxItems);
        }

        copyDeque(rhs);
        return *this;
    }

private:
    int numItems; // how many items are currently in the Deque?
    int maxItems; // how many items can I put on the Deque before full?
    int myBack;
    int myFront;
    T * data; // dynamically allocated array of T that contains our data

    // copies deques when needed
    void copyDeque(const Deque<T> & rhs);

    // important for wrapping around the deque when needed
    T & accessData(int index) throw(const char *) {
        if (!empty()) {
            return data[index];
        }
        else {
            throw "ERROR: unable to access data from an empty deque";
        }
    }

    // allocates the space we need
    void allocate(int Space) throw(const char *);
};

/*******************************************
* CONTAINER::COPY CONSTRUCTOR
*******************************************/
template <class T>
Deque <T>::Deque(const Deque <T> &rhs) throw(const char *) {
    assert(rhs.maxItems >= 0);

    // do nothing if there is nothing to do
    if (rhs.maxItems == 0) {
        maxItems = numItems = 0;
        data = NULL;
        myFront = myBack = 0;
        return;
    }

    // attempt to allocate
    allocate(rhs.maxItems);

    // copy over the maxItems and size
    assert(rhs.numItems >= 0 && rhs.numItems <= rhs.maxItems);
    this->maxItems = rhs.maxItems;
    this->numItems = rhs.numItems;
    this->myFront = rhs.myFront;
    this->myBack = rhs.myBack;

    int index = myFront;

    // copy the items over one at a time using the assignment operator
    for (int i = 0; i < numItems; i++) {
        data[index] = rhs.data[index];
        index = (index + 1) % maxItems;
    }
}

/**********************************************
* CONTAINER : NON-DEFAULT CONSTRUCTOR
* Preallocate the container to "maxItems"
**********************************************/
template <typename T>
Deque <T>::Deque(int p_Max) throw(const char *) {
    assert(maxItems >= 0);

    // do nothing if there is nothing to do
    if (p_Max == 0) {
        this->maxItems = this->numItems = 0;
        this->data = NULL;
        this->myFront = this->myBack = 0;
        return;
    }

    // attempt to allocate
    allocate(p_Max);

    // copy over the stuff
    this->maxItems = p_Max;
    this->numItems = 0;

    // set the front and the back of the deque
    this->myFront = this->myBack = 0;

    // initialize deque through ourconstructor
    for (int i = 0; i < p_Max; i++) {
        data[i] = T();
    }
}
/**********************************************
* CONTAINER : FRONT
* returns item in front of Deque
**********************************************/
template <typename T>
const T &Deque<T>::front() throw(const char *) {
    // TODO: make sure this is pointing to the last item added somehow
    if (empty()) {
        throw "ERROR: unable to access data from an empty deque";
    }
    else {
        return accessData(myFront);
    }
}
/**********************************************
* CONTAINER : PUSH_FRONT
* allows new values to be placed in Deque
**********************************************/
template <typename T>
void Deque<T>::push_front(const T &t) throw(const char *) {
    int newFront;

    if (numItems == 0 && maxItems != 0) {
        myFront = 0;
        myBack = 0;
    }

    if (maxItems == 0) {
        maxItems = 4;
        allocate(maxItems);
        newFront = myFront % maxItems;
    }
    else if (numItems >= maxItems) {
        int old_maxItems = maxItems;
        maxItems *= 2;
        newFront = myFront % maxItems;

        T * temp;

        try
        {
            temp = new T[maxItems];
        }
        catch(std::bad_alloc) {
            throw "ERROR: Unable to allocate a new buffer for deque";
        }

        int index = myFront;

        // copy over with assignment operator
        for (int i = 0; i < numItems; i++) {
            temp[i]  = data[(index + i) % old_maxItems];
        }

        delete [] data;
        data = temp;
        myFront = 0;
        myBack = numItems; // we are sure this exists in the deque
    }
    else if (numItems == 0) {
        data[myFront] = t;
        numItems++;
        return;
    }

    myFront = (myFront - 1 + maxItems) % maxItems;
    data[myFront] = t;
    numItems++;

    return;
}
/**********************************************
* POP_FRONT
* removes an element from the front of the Deque
**********************************************/
template <typename T>
void Deque<T>::pop_front() throw(const char *) {
    if (!empty()) {
        myFront = (myFront + 1) % maxItems;
        numItems--;
    }
    else {
        throw "ERROR: unable to pop from the front of empty deque";
    }
};

/**********************************************
* CONTAINER : BACK
* returns item in back of Deque
**********************************************/
template <typename T>
const T &Deque<T>::back() throw(const char *) {
    if (myBack != 0) {
        return accessData(myBack - 1);
    }
    else {
        return accessData(myBack);
    }
};
/**********************************************
* CONTAINER : BACK
* allocates the needed space so we don't break
* things
**********************************************/
template <class T>
void Deque <T>::allocate(int space) throw(const char *) {
    // attempt to allocate
    try
    {
        data = new T[space];
    }
    catch(std::bad_alloc) {
        throw "ERROR: Unable to allocate a new buffer for deque";
    }
};

/**********************************************
* CONTAINER : PUSH_BACK
* allows new values to be placed in Deque
**********************************************/
template <typename T>
void Deque<T>::push_back(const T &t) throw(const char *) {
    int newBack;

    if (numItems == 0 && maxItems != 0) {
        myFront = 0;
        myBack = 0;
    }

    if (maxItems == 0) {
        maxItems = 4;
        allocate(maxItems);
        newBack = (myBack + 1) % maxItems;
    }
    else if (numItems >= maxItems) {
        int old_maxItems = maxItems;
        maxItems *= 2;

        T * temp;

        try
        {
            temp = new T[maxItems];
        }
        catch(std::bad_alloc) {
            throw "ERROR: Unable to allocate a new buffer for Deque";
        }

        int index = myFront;

        // copy over with assignment operator
        for (int i = 0; i < numItems; i++) {
            temp[i] = data[index];
            index = (index + 1) % old_maxItems;
        }

        delete [] data;
        data = temp;
        myFront = 0;
        myBack = numItems; // we are sure this exists in the deque
    }
    else if (numItems == 0) {
        myBack = 0;
    }

    data[myBack] = t;
    myBack = (myBack + 1) % maxItems;
    numItems++;

    return;
}

/**********************************************
* POP_BACK
* removes an element from the back of the Deque
**********************************************/
template <typename T>
void Deque<T>::pop_back() throw(const char *) {
    if (empty()) {
        throw "ERROR: unable to pop from the back of empty deque";
    }
    else {
        myBack = (myBack - 1 + maxItems) % maxItems;
        numItems--;
    }
};
/**********************************************
* EMPTY
* checks to see if the Deque is empty
**********************************************/
template <typename T>
bool Deque<T>::empty() const {
    return numItems == 0;
}

/**********************************************
* COPY DEQUE
* allows us to copy deques when needed to
* keep our data straight
**********************************************/
template <typename T>
void Deque <T>::copyDeque(const Deque<T> &rhs) {
    assert(rhs.numItems >= 0 && rhs.numItems <= rhs.maxItems);
    numItems = rhs.numItems;

    maxItems = rhs.maxItems;

    myFront = rhs.myFront;
    myBack = rhs.myBack;

    int index = myFront;

    for (int i = 0; i < numItems; i++) {
        data[index] = rhs.data[index];
        index = (index + 1) % maxItems;
    }
}

#endif /* Deque_h */
