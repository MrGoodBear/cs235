/***********************************************************************
 * Header:
 *    NOW SERVING
 * Summary:
 *    This will contain just the prototype for nowServing(). You may
 *    want to put other class definitions here as well.
 * Author
 *    Cameron Lewis
 ************************************************************************/

#ifndef NOW_SERVING_H
#define NOW_SERVING_H

#include <string>
#include <iostream>

#include "deque.h"     // for DEQUE

// the interactive nowServing program
void nowServing();

/******************************************
*  CLASS : Student
*
*  Contains all the information we need
*  to know about each student
******************************************/
class Student {
private:
    // student info
    string name;
    string className;
    int timeNeeded;

public:
    // default constructor
    Student() : timeNeeded(0) {
    };

    // GETTERS AND SETTERS
    // to protect our data :)

    // getters
    string getClass() {return className;}
    string getName() {return name;}
    int getTime() {return timeNeeded;}

    // setters
    void setClass(string className) {this->className = className;}
    void setName(string name) {this->name = name;}
    void setTimeNeeded(int timeNeeded) {this->timeNeeded = timeNeeded;}
};

/******************************************
*  CLASS : HelpRequest
*
*  Contains everything required to manage
*  student help requests
******************************************/
class HelpRequest {
private:
    // these deques contain all our student info
    Deque<string> ClassNames;
    Deque<string> Names;
    Deque<int> Times;

    // these store the info of the student currently being helped
    string name;
    string className;
    int timeLeft;
    bool priority;
    string input;
public:
    // a simple default constructor
    HelpRequest() : timeLeft(0), priority(false) {
    };

    // pushes requests into the system
    void pushRequest(Student &student);

    // this function is crucial to making sure students'
    //  emergencies were recognized
    void setInput(string input) {
        this->input = input;
    }

    // displays system queue
    void display(Student &student);
};

#endif // NOW_SERVING_H
