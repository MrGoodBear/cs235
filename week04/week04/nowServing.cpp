/***********************************************************************
 * Implementation:
 *    NOW SERVING
 * Summary:
 *    This will contain the implementation for nowServing() as well as any
 *    other function or class implementations you may need
 * Author
 *    Cameron Lewis
 **********************************************************************/

#include <iostream>     // for ISTREAM, OSTREAM, CIN, and COUT
#include <string>       // for STRING
#include <cassert>      // for ASSERT
#include "nowServing.h" // for nowServing() prototype
#include "deque.h"      // for DEQUE
using namespace std;

/************************************************
 * NOW SERVING
 * The interactive function allowing the user to
 * handle help requests in the Linux lab
 ***********************************************/
void nowServing() {
    // instructions
    cout << "Every prompt is one minute.  The following input is accepted:\n";
    cout << "\t<class> <name> <#minutes>    : a normal help request\n";
    cout << "\t!! <class> <name> <#minutes> : an emergency help request\n";
    cout << "\tnone                         : no new request this minute\n";
    cout << "\tfinished                     : end simulation\n";

    // create needed class objects
    Student student;
    HelpRequest request;

    // variables to parse input and pass to class objects
    string initial_input;
    string className;
    string name;
    int timeNeeded;
    bool finished = false;
    int minutes = 0;

    // loops until user says they're finished!
    do {
        cout << "<" << minutes << "> ";
        cin >> initial_input;
        if (initial_input == "finished") {
            finished = true;
            break;
        }
        else if (initial_input == "none") {
            request.display(student);
            minutes++;
            continue;
        }
        // this means there's an emergency!
        else if (initial_input == "!!") {
            cin >> className >> name >> timeNeeded;
            student.setClass(className);
            student.setName(name);
            student.setTimeNeeded(timeNeeded);
            request.pushRequest(student);
            request.setInput(initial_input);
        }
        // no emergencies or special input
        else {
            className = initial_input;
            cin >> name >> timeNeeded;
            student.setClass(className);
            student.setName(name);
            student.setTimeNeeded(timeNeeded);
            request.pushRequest(student);
        }

        request.display(student);
        minutes++;
    } while (finished == false);

    // end
    cout << "End of simulation\n";
}

/*********************************************
* MEMBER FUNCTION : pushRequest
*
* Reponsible for pushing student's requests
* into the system
*********************************************/
void HelpRequest :: pushRequest(Student & student) {
    string request;

    ClassNames.push_front(student.getClass());
    Names.push_front(student.getName());
    Times.push_front(student.getTime());
}

/*********************************************
* MEMBER FUNCTION : Display
*
* Shows the ticking progress of each student
* in the system's queue
*********************************************/
void HelpRequest :: display(Student & student) {
    // this if statement removes helped students out of the queue
    //   and brings new ones into it
    if (timeLeft == 0 && !ClassNames.empty()) {
        className = ClassNames.front();
        name = Names.front();
        timeLeft = Times.front();

        // emergencies
        if (input == "!!") {
            priority = true;
            input = "";
        }

        ClassNames.pop_front();
        Names.pop_front();
        Times.pop_front();
    }

    // displays all our info, whether there's
    // an emergency or not
    if (timeLeft != 0) {
        if (priority == true) {
            cout << "\tEmergency for " << name;
            priority = false;
        }
        else {
            cout << "\tCurrently serving " << name;
        }

        cout << " for class " << className
             << ". Time left: " << timeLeft << "\n";
        timeLeft--;
    }
}
