/***********************************************************************
 * Header:
 *    Vector
 * Summary:
 *    This class contains the notion of a container: a bucket to hold
 *    data for the user. This is just a starting-point for more advanced
 *    constainers such as the vector, set, stack, queue, deque, and map
 *    which we will build later this semester.
 *
 *    This will contain the class definition of:
 *        Vector         : A class that holds stuff
 *        VectorIterator : An interator through Vector
 * Author
 *    Cameron Lewis
 ************************************************************************/

#ifndef CONTAINER_H
#define CONTAINER_H
#include <cassert>
#include <cstddef>
#include <iostream>
#include <new>
#include <string>

// forward declaration for VectorIterator
template <typename T> class VectorIterator;

/************************************************
 * VECTOR
 * A container class that holds stuff
 ***********************************************/
template <typename T> class Vector {
public:
  // default constructor : empty and kinda useless
  Vector() : numItems(0), maxItems(0), data(new T) {}

  // copy constructor : copy it
  Vector(const Vector &rhs);

  // non-default constructor : pre-allocate
  Vector(int maxItems) throw(const char *);

  // destructor : free everything
  ~Vector() {
    if (maxItems)
      data = NULL;
  }

  void push_back(const T &value) {

    // if capacity is empty, set capacity to 1
    if (maxItems == 0) {
      ++maxItems;
    }

    // if capacity is full, double capacity and reallocate
    if (maxItems == numItems) {
      Vector<T> new_data(1);
      try {
        new_data.maxItems = maxItems;
        new_data.data = new T[(maxItems)];
        new_data.numItems = numItems;

        for (int i = 0; i < numItems; i++) {
          new_data.data[i] = data[i];
        }

        data = new (std::nothrow) T[(maxItems *= 2)];

        for (int i = 0; i < numItems; i++) {
          data[i] = new_data.data[i];
        }
      } catch (std::bad_alloc) {
        throw "ERROR: Unable to allocate a new buffer for Vector";
      }
    }

    // add item to vector and increase counter
    data[numItems] = value;
    numItems++;
  }

  bool empty() const {
    if (numItems != 0)
      return false;
    else
      return true;
  }

  // remove all the items from the container
  void clear() { numItems = 0; }

  // how many items are currently in the container?
  int size() const { return numItems; }

  // returns the current capacity of the Vector
  int capacity() { return maxItems; }

  // return an iterator to the beginning of the list
  VectorIterator<T> begin() { return VectorIterator<T>(data); }

  // return an iterator to the end of the list
  VectorIterator<T> end() { return VectorIterator<T>(data + numItems); }

  // overloaded subscript operator. Returns by-reference
  T &operator[](int index) {
    return data[index];
    if (index > numItems) {
      throw "ERROR: Invalid index";
    }
    return data[index];
  }

  // overloaded subscript operator. Const, and returns const-by-reference
  const T &operator[](int index) const {
    return data[index];
    if (index > numItems) {
      throw "ERROR: Invalid index";
    }
    return data[index];
  }

  // overloaded assignment operator
  Vector<T> operator=(const Vector rhs) {

    // do nothing if there is nothing to do
    if (rhs.numItems == 0) {
      maxItems = numItems = 0;
      data = NULL;
    }

    if (numItems != rhs.maxItems) {
      // attempt to allocate
      maxItems = rhs.maxItems;
      numItems = rhs.numItems;
      try {
        data = new T[maxItems];
      } catch (std::bad_alloc) {
        throw "ERROR: Unable to allocate buffer";
      }
    }

    // copy the items over one at a time using the default assignment operator
    for (int i = 0; i < numItems; i++)
      data[i] = rhs.data[i];

    return *this;
  }

private:
  int numItems; // how many items are currently in the Vector?
  int maxItems; // how many items can I put on the Vector before full?
  T *data;      // dynamically allocated array of T
};

/**************************************************
 * CONTAINER ITERATOR
 * An iterator through Vector
 *************************************************/
template <class T> class VectorIterator {
public:
  // default constructor
  VectorIterator() : p(NULL) {}

  // initialize to direct p to some item
  VectorIterator(T *p) : p(p) {}

  // copy constructor
  VectorIterator(const VectorIterator &rhs) { *this = rhs; }

  // assignment operator
  VectorIterator &operator=(const VectorIterator &rhs) {
    this->p = rhs.p;
    return *this;
  }

  // not equals operator
  bool operator!=(const VectorIterator &rhs) const { return rhs.p != this->p; }

  // dereference operator
  T &operator*() { return *p; }

  // prefix increment
  VectorIterator<T> &operator++() {
    p++;
    return *this;
  }

  // postfix increment
  VectorIterator<T> operator++(int postfix) {
    VectorIterator tmp(*this);
    p++;
    return tmp;
  }

private:
  T *p;
};

/*******************************************
 * CONTAINER :: COPY CONSTRUCTOR
 *******************************************/
template <class T> Vector<T>::Vector(const Vector<T> &rhs) {
  maxItems = rhs.maxItems;
  numItems = rhs.numItems;
  data = new T[maxItems];
  try {
    if (data != 0) {
      for (int pos = 0; pos <= numItems; pos++)
        data[pos] = rhs.data[pos];
    }
  } catch (std::bad_alloc) {
    throw "ERROR: Unable to allocate a new buffer for Vector";
  }
}

/**********************************************
 * CONTAINER : NON-DEFAULT CONSTRUCTOR
 * Preallocate the container to "maxItems"
 **********************************************/
template <class T> Vector<T>::Vector(int maxItems) throw(const char *) {
  assert(maxItems >= 0);

  // do nothing if there is nothing to do
  if (maxItems == 0) {
    this->maxItems = this->numItems = 0;
    this->data = NULL;
    return;
  }

  // attempt to allocate
  try {
    data = new T[maxItems];
  } catch (std::bad_alloc) {
    throw "ERROR: Unable to allocate a new buffer for Vector";
  }

  // copy over the stuff
  this->maxItems = maxItems;
  this->numItems = 0;

  // initialize the container by calling the default constructor
  for (int i = 0; i < maxItems; i++)
    data[i] = T();
}

#endif // CONTAINER_H
