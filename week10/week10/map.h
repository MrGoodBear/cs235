/***********************************************************************
 * Header:
 *    MAP
 * Summary:
 *    Class MAP - Has functionality similar to the standard map data type
 * Author
 *    Cameron Lewis
 ************************************************************************/

#include "bst.h"
#include "pair.h"
#include "stack.h"
#ifndef MAP_H
#define MAP_H

//forward declaration
template<class K, class V>
class MapIterator;

template<class K, class V>
class Map {
public:
   // default constructor
   Map(): bst(), numItems(0){};

   // copy constructor
   Map (Map <K,V> & rhs)   {
      *this = rhs;
   }

   // destructor
   ~Map(){bst.~BST();};

   // Iterators
   MapIterator<K,V> begin();
   MapIterator<K,V> end()             {return MapIterator < K,V > (NULL);};
   MapIterator<K,V> rbegin();
   MapIterator<K,V> rend()            {return MapIterator < K,V > (NULL);};

   //  Square Bracket overload - handles inserts and data access
   V & operator [] (const K & key)
   {
      Pair<K,V> pair(key, V());

      BSTIterator<Pair<K,V > > it = bst.find(pair);
      if(it == NULL)
      {
         bst.insert(pair);
         numItems++;
         it = bst.find(pair);
      }
      return  it.getNode()->data.second;
   }

   // assignment operatore overload for Map objects
   Map<K,V> & operator = ( Map<K,V>  & rhs) throw (const char *);

   // member functions

   // function that calls the clear function of the BST object and resets teh numItems variable
   void clear()
   {
      this->bst.clear();
      this->numItems = 0;
   };

   // function calls the empty function of the BST class to see if MAP/BST is empty
   bool empty(){return bst.empty();};

   // getter setter
   int getnumItems(){return numItems;};
   void incNumItems(){numItems++;};
   int size(){return numItems;};
   BST < Pair < K, V > > * getBST() {return &bst;};

private:
   // number of items in MAP/BST
   int numItems;
   BST < Pair < K, V > > bst;
};

///*****************************************
// * OPERATOR <<
// * Display the contents of the BNODE
// ****************************************/
template <class K, class V>
ostream & operator << ( ostream & out , BinaryNode < Pair < K,V > > p)
{
   if (&p != NULL) {
       out << p.data.second;
   }
   return out;
}

/**********************************************************
* MAP Iterator
* Forward and reverse iterator through a MAP
*********************************************************/
template <class K, class V>
class MapIterator
{
public:
   // default constructor
   MapIterator():it(NULL){};

   // 3 Non-default constructors
   MapIterator(BinaryNode <Pair < K,V > > * s) { it = s; }
   MapIterator(BSTIterator<  Pair < K, V > > s) : it(0) { it = s; };
   MapIterator(const MapIterator <K,V> & rhs)  { it = rhs.it; };

   // Copy Constructor
   MapIterator(MapIterator<K,V> & copy) { *this = copy; };

   // comparison operator overload - only need to compare the leaf node
   bool operator == (const MapIterator <K,V> & rhs) const
   {
      return rhs.it.operator*() == it.operator*();
   }

   // operator overload - only need to compare the leaf node
   bool operator != (const MapIterator <K,V> & rhs) const
   {
      return this->it.operator!=(rhs.it);
   }

   // Calls the increment operator overload on on iterator variable
   const MapIterator < K , V > & operator ++ ()
   {
      ++(this->it);
      return *this;
   };

   // Calls the increment operator overload on iterator variable
   const MapIterator < K , V > & operator -- ()
   {
      --(this->it);
      return *this;
   };

   // de-reference. Cannot change because it will invalidate the BST
   const  V     & operator * () const
   {
      return this->it.operator*().getSecond();
   }

private:

   BSTIterator<  Pair < K, V > > it;
};


///*****************************************************
//* BST :: BEGIN
//* Return the first node (left-most) in a binary search tree
//****************************************************/
template <class K, class V>
MapIterator<K,V> Map<K,V>:: begin()
{
   return MapIterator<K,V>(BSTIterator< Pair < K,V > > (bst.begin()));
}

/*****************************************************
* BST :: RBEGIN
* Return the last node (right-most) in a binary search tree
****************************************************/
template <class K, class V>
MapIterator<K,V>  Map<K,V> :: rbegin()
{
   return MapIterator<K,V>(BSTIterator< Pair < K,V > > (bst.rbegin()));
}

/**********************************************
* BST :: COPY
* Make a copy of a binary search tree
**********************************************/
template <class K, class V>
Map<K,V> & Map<K,V>:: operator = ( Map<K,V>  & rhs) throw (const char *)
{
   this->clear();
   if ( rhs.bst.begin() != NULL)
   {
      try
      {
         this->bst = rhs.bst;
         this->numItems = rhs.numItems;
      }
      catch (...)
      {
         throw "ERROR: Unable to allocate a node";
      }
   }
   return *this;
}


#endif //MAP_H
