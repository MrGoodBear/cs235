/***********************************************************************
* Header:
*    WORD COUNT
* Summary:
*    This will contain just the prototype for the wordCount()
*    function
* Author
*     Lewis
************************************************************************/

#ifndef WORD_COUNT_H
#define WORD_COUNT_H
#include "pair.h" // for pair class
#include "map.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;
/*****************************************************
 * WORD COUNT
 * Prompt the user for a file to read, then prompt the
 * user for words to get the count from
 *
 *****************************************************/
void wordCount();

#endif // WORD_COUNT_H

