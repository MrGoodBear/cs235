/***********************************************************************
 * Module:
 *    Week 10, WORD COUNT
 *    Brother Helfrich, CS 235
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This program implements wordCount()
 ************************************************************************/


#include "wordCount.h" // for wordCount() prototype


void readFile(Map <string, int> & counts, const string & fileName);

/*****************************************************
 * WORD COUNT
 * Prompt the user for a file to read, then prompt the
 * user for words to get the count from
 *****************************************************/
void wordCount()
{
  string fileName;
  string search;
  int count;
  Map<string, int> tree;

  //get a filename
  cout << "What is the filename to be counted? ";
  cin.ignore();
  getline(cin, fileName);

  readFile(tree, fileName);

  cout << "What word whose frequency is to be found. Type ! when done\n";

  while (search != "!") {
    cin.clear();
    cout << "> ";
    getline(cin, search);
    Pair<string,int> pair(search, 0);
    MapIterator < string, int > it = tree.getBST()->find(pair);
    if(it != tree.end())
      cout << "\t" << search << " : " << tree[search] << endl;
    else if(search != "!")
      cout << "\t" << search << " : 0\n";
  }
}


void readFile(Map <string, int> & counts, const string & fileName)
{
  string data;
  ifstream fin(fileName.c_str());

  if (fin.is_open())  {
    while(fin >> data) {
      Pair<string,int> pair(data, 0);
      MapIterator < string, int > it;

      if(!counts.empty()) {
        it = counts.getBST()->find(pair);

        if(it != counts.getBST()->end())
          counts[data] += 1;
        else
          counts.getBST()->insert(Pair<string, int>(data, 1));
          counts.incNumItems();
      }
      else
        counts.getBST()->insert(Pair<string, int>(data, 1));
      counts.incNumItems();
    }
  }

  fin.close();

  return;
}
