/***********************************************************************
 * Component:
 * Week 09, Binary Search Tree (BST)
 * Brother Helfrich, CS 235
 *      Author:
 *          Cameron Lewis
 * Summary:
 * Creates a binary search tree
 ************************************************************************/
#ifndef BST_H
#define BST_H
#include "bnode.h" // for BinaryNode

#include "stack.h" // for Stack
// forward declaration for the BST iterator
template <class T>
class BSTIterator;
/*****************************************************************
 * BINARY SEARCH TREE
 * Create a Binary Search Tree
 *****************************************************************/
template <class T>
class BST
{
public:
    // constructors, destructor, assignment operator
    BST() : root(NULL) { }
    BST(const BST<T> & rhs) throw (const char *);
    ~BST() { clear(); }
    BST & operator = (const BST & rhs) throw (const char *);
    
    
    bool empty() const { return root == NULL; }
    int size() const { return empty() ? 0 : root->size(); }
    
    // iterators
    BSTIterator <T> begin();
    BSTIterator <T> end() { return BSTIterator <T> (NULL); }
    BSTIterator <T> rbegin();

    
    void clear()
    {
        if (root)
            deleteBinaryTree(root);
        root = NULL;
        assert(empty());
    }
    // BST specific interfaces
    void insert(const T & t) throw (const char * );
    void remove(BSTIterator <T> & it);
    BSTIterator <T> find(const T & t);
        BSTIterator <T> rend() { return BSTIterator <T> (NULL); }
    void copyTree(BinaryNode<T> *& copiedNode, BinaryNode<T> * node)
    {
        if(node == NULL)
            copiedNode = NULL;
        else {
            copiedNode = new BinaryNode<T>;
            copiedNode->data = node->data;
            copiedNode->pParent = node->pParent;
            copyTree(copiedNode->pLeft, node->pLeft);
            copyTree(copiedNode->pRight, node->pRight);
        }
    }
    BinaryNode <T> * getRoot () {return root;};
private:
    // removes/deletes a single node from tree
    void deleteNode (BinaryNode <T> * & pDelete, bool toRight);
    BinaryNode <T> * root;
};
/**********************************************************
 * BST ITERATOR
 * A forward and reverse iterator through tree
 *********************************************************/
template <class T>
class BSTIterator
{
public:
    // constructors
    BSTIterator(BinaryNode <T> * p = NULL){nodes.push(p);}
    BSTIterator(Stack <BinaryNode <T> *> & s) { nodes = s; }
    BSTIterator(const BSTIterator <T> & rhs) { nodes = rhs.nodes; }
    BSTIterator <T> & operator = (const BSTIterator <T> & rhs)
    {
        nodes = rhs.nodes;
        return *this;
    }
    bool operator == (const BSTIterator <T> & rhs) const
    {
        return rhs.nodes.const_top() == nodes.const_top();
    }
    bool operator != (const BSTIterator <T> & rhs) const
    {
        return rhs.nodes.const_top() != nodes.const_top();
    }
    const T & operator * () const
    {
        return nodes.const_top()->data;
    }
    // iterators
    BSTIterator <T> & operator ++ ();
    BSTIterator <T> operator ++ (int postfix)
    {
        BSTIterator <T> itReturn = *this;
        ++(*this);
        return itReturn;
    }
    BSTIterator <T> & operator -- ();
    BSTIterator <T> operator -- (int postfix)
    {
        BSTIterator <T> itReturn = *this;
        --(*this);
        return itReturn;
    }
    // must give friend status
    friend void BST <T> :: remove(BSTIterator <T> & it);
    BinaryNode <T> * getNode() { return nodes.top(); }
private:
    // stack of nodes
    Stack < BinaryNode <T> * > nodes;
};

/*********************************************
 * COPY CONSTRUCTOR
 * Copies A tree to another tree
 ********************************************/
template <class T>
BST <T> :: BST ( const BST <T> & rhs) throw (const char *) : root(NULL)
{
    *this = rhs;
}

template <class T>
BST <T> & BST <T> :: operator = (const BST <T> & rhs) throw (const char *)
{
    clear();
    if (rhs.root != NULL)
    {
        try
        {
            this->root = new BinaryNode <T> (rhs.root->data);
            copyTree(this->root, rhs.root);
        }
        catch (...)
        {
            throw "ERROR: Unable to allocate a node";
        }
    }
    return *this;
}

/*****************************************************
 * BST :: RBEGIN
 * Return the last right-most node in tree
 ****************************************************/
template <class T>
BSTIterator <T> BST <T> :: rbegin()
{
    Stack < BinaryNode <T> * > nodes;
    nodes.push(NULL);
    nodes.push(root);
    while (nodes.top() != NULL && nodes.top()->pRight)
        nodes.push(nodes.top()->pRight);
    return BSTIterator <T> (nodes);
}

/*****************************************************
 * BEGIN
 ****************************************************/
template <class T>
BSTIterator <T> BST <T> :: begin()
{
    Stack < BinaryNode <T> * > nodes;
    nodes.push(NULL);
    nodes.push(root);
    while (nodes.top() != NULL && nodes.top()->pLeft)
        nodes.push(nodes.top()->pLeft);
    return BSTIterator <T> (nodes);
}

/*****************************************************
 * INSERT
 ****************************************************/
template <class T>
void BST <T> :: insert(const T & item) throw (const char *)
{
    try
    {
        BinaryNode<T> * locptr = root;
        BinaryNode<T> * parent = 0;
        
        while (locptr != NULL  ) {
            parent = locptr;
            if (item < locptr->data)
                locptr = locptr->pLeft;
                else
                    locptr = locptr->pRight;
                    }
        
        locptr = new BinaryNode<T>(item);
        if(parent == NULL)
        {
            root = locptr;
            root->isRed = false;
        }
        else if (item < parent->data)
        {
            parent->pLeft = locptr;
            locptr->pParent = parent;
            
            if (locptr->pParent != NULL && locptr->pParent->pParent !=NULL && locptr->pParent->pParent->pRight != NULL)
                locptr->pAunt = locptr->pParent->pParent->pRight;
                }
        else
        {
            parent->pRight = locptr;
            locptr->pParent = parent;
            
            if (locptr->pParent != NULL && locptr->pParent->pParent !=NULL && locptr->pParent->pParent->pLeft != NULL)
                locptr->pAunt = locptr->pParent->pParent->pLeft;
                
                }
        locptr->balance();
    }
    catch(...)
    {
        throw "ERROR: Unable to allocate a node";
    }
}
/****************************************************
 * find
 ****************************************************/
template <class T>
BSTIterator <T> BST <T> :: find(const T & t)
{
    Stack < BinaryNode <T> * > nodesFound;
    nodesFound.push(root);
    // non-recurive solution
    while (nodesFound.top() != NULL)
    {
        if (nodesFound.top()->data == t)
            return nodesFound;
        if (nodesFound.top()->data > t)
            nodesFound.push(nodesFound.top()->pLeft);
        else
            nodesFound.push(nodesFound.top()->pRight);
    }
    return BSTIterator <T>(NULL);
}


/****************************************************
 * deleteNode
 * Deletes a single node (pDelete) from the tree
 ****************************************************/
template <class T>
void BST <T> :: deleteNode(BinaryNode <T> * & pDelete, bool toRight)
{
    // shift everything up
    BinaryNode <T> * pNext = (toRight ? pDelete->pRight : pDelete->pLeft);
    // if we are not the parent, hook ourselves into the existing tree
    if (pDelete != root)
    {
        if (pDelete->pParent->pLeft == pDelete)
        {
            pDelete->pParent->pLeft = NULL;
            pDelete->pParent->addLeft(pNext);
        }
        else
        {
            pDelete->pParent->pRight = NULL;
            pDelete->pParent->addRight(pNext);
        }
    }
    // otherwise, the pNext is the new root
    else
    {
        root = pNext;
        pNext->pParent = NULL;
    }
}
/*************************************************
 * REMOVE
 * Removes a node given by the iterator
 ************************************************/

template <class T>
void BST <T> :: remove(BSTIterator <T> & it)
{
    // find the node
    BinaryNode <T> * pNode = it.getNode();
    // do nothing if there is nothing to do
    if (pNode == NULL)
        return;
    // if there is only one child (right) or no children (how sad!)
    if (pNode->pLeft == NULL)
        deleteNode(pNode, true /* goRight */);
    // if there is only one child (left)
    else if (pNode->pRight == NULL)
        deleteNode(pNode, false /* goRight */);
    // otherwise, swap places with the in-order successor
    else
    {
        // find the in-order successor
        BinaryNode <T> * pNodeIOS = pNode->pRight;
        while (pNodeIOS->pLeft != NULL)
            pNodeIOS = pNodeIOS->pLeft;
        // copy its data
        pNode->data = pNodeIOS->data;
        // if there are any children under the in-order successor, fix them
        assert(pNodeIOS->pLeft == NULL); // there cannot be a left child or
        // I would not be the IOS
        deleteNode(pNodeIOS, true /*goRight*/);
        // prepare for deletion
        pNode = pNodeIOS;
    }
    delete pNode;
}


/**************************************************
 * BST ITERATOR
 * advance by one
 *************************************************/
template <class T>
BSTIterator <T> & BSTIterator <T> :: operator -- ()
{
    // do nothing if we have nothing
    if (nodes.top() == NULL)
        return *this;
    if (nodes.top()->pLeft != NULL)
    {
        nodes.push(nodes.top()->pLeft);
        while (nodes.top()->pRight)
            nodes.push(nodes.top()->pRight);
        return *this;
    }
    assert(nodes.top()->pLeft == NULL);
    BinaryNode <T> * pSave = nodes.top();
    nodes.pop();
    if (NULL == nodes.top())
        return *this;
    if (pSave == nodes.top()->pRight)
        return *this;
    while (nodes.top() != NULL && pSave == nodes.top()->pLeft)
    {
        pSave = nodes.top();
        nodes.pop();
    }
    return *this;
}

/**************************************************
 * BST ITERATOR
 * advance by one
 *************************************************/

template <class T>
BSTIterator <T> & BSTIterator <T> :: operator ++ ()
{
    // do nothing if we have nothing
    if (nodes.top() == NULL)
        return *this;
    if (nodes.top()->pRight != NULL)
    {
        nodes.push(nodes.top()->pRight);
        while (nodes.top()->pLeft)
            nodes.push(nodes.top()->pLeft);
        return *this;
    }
    assert(nodes.top()->pRight == NULL);
    BinaryNode <T> * pSave = nodes.top();
    nodes.pop();
    if (NULL == nodes.top())
        return *this;
    if (pSave == nodes.top()->pLeft)
        return *this;
    while (nodes.top() != NULL && pSave == nodes.top()->pRight)
    {
        pSave = nodes.top();
        nodes.pop();
    }
    return *this;
}


#endif // BST_H
