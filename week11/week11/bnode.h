/***********************************************************************
 * Header:
 *    BNODE
 * Summary:
 *    Class BNODE
 * Author
 *    Cameron Lewis
 ************************************************************************/

#include <iostream>
#include <string>
#include <cassert>
using namespace std;

#ifndef BNODE_H
#define BNODE_H

template <class T>
class BinaryNode {
 public:
  // default constructor
  BinaryNode() throw(const char*) {
    try {
      pLeft = NULL;
      pRight = NULL;
      pParent = NULL;
      pAunt = NULL;
      grandParent = NULL;
      pSibling = NULL;
      isRed = true;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  };

  //  non-default constructor
  BinaryNode(T input) throw(const char*) {
    try {
      pLeft = NULL;
      pRight = NULL;
      pParent = NULL;
      pAunt = NULL;
      grandParent = NULL;
      pSibling = NULL;
      isRed = true;
      data = input;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  };

  BinaryNode(const BinaryNode<T>& rhs) throw(const char*) {
    try {
      this->pRight = NULL;
      this->pLeft = NULL;
      this->pParent = NULL;
      grandParent = NULL;
      pSibling = NULL;
      *this = rhs;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  }
  void verifyRedBlack(int depth) const;
  void verifyBTree() const;
  void balance();

  // returns length of the List
  int size() { return sizeAux(this); };
  int sizeAux(BinaryNode<T>* node) {
    if (node == NULL)
      return (0);
    else
      return (sizeAux(node->pLeft) + 1 + sizeAux(node->pRight));
  }
  // add right
  void addRight(const T rightNode) throw(const char*);
  void addRight(BinaryNode<T>* rightNode);

  // add left
  void addLeft(const T leftNode) throw(const char*);
  void addLeft(BinaryNode<T>* leftNode);

  int findDepth() const;

  void rotateRight(BinaryNode<T>* tree);

  T data;
  bool isRed;
  BinaryNode<T>* pLeft;
  BinaryNode<T>* pRight;
  BinaryNode<T>* pParent;
  BinaryNode<T>* pAunt;
  BinaryNode<T>* pSibling;
  BinaryNode<T>* grandParent;
};

/*****************************************
 * BINARYNODE :: ADDRIGHT
 ****************************************/
template <class T>
void BinaryNode<T>::addRight(const T rightNode) throw(const char*) {
  try {
    BinaryNode<T>* pNew = new BinaryNode<T>(rightNode);
    pNew->pParent = this;
    pNew->isRed = true;
    pRight = pNew;
  } catch (...) {
    throw "ERROR: Unable to allocate a node";
  }
}

/*****************************************
 * ADDRIGHT
 ****************************************/
template <class T>
void BinaryNode<T>::addRight(BinaryNode<T>* rightNode) {
  pRight = rightNode;
}

/*****************************************
 * OPERATOR <<
 * Displays the contents of BNODE
 ****************************************/
template <class T>
ostream& operator<<(ostream& out, BinaryNode<T>* pStart) {
  if (pStart != NULL) {
    cout << pStart->pLeft;
    out << pStart->data << " ";
    cout << pStart->pRight;
  }

  return out;
}

/*****************************************
 * ADDLEFT
 ****************************************/
template <class T>
void BinaryNode<T>::addLeft(const T leftNode) throw(const char*) {
  try {
    BinaryNode<T>* pNew = new BinaryNode<T>(leftNode);
    pNew->pParent = this;
    pNew->isRed = true;
    pLeft = pNew;
  } catch (...) {
    throw "ERROR: Unable to allocate a node";
  }
}

/*****************************************
 * ADDLEFT
 * Adds a BinaryNode to the left
 ****************************************/
template <class T>
void BinaryNode<T>::addLeft(BinaryNode<T>* leftNode) {
  pLeft = leftNode;
}

/*****************************************
 * DELETEBINARYTREE
 * Recursively deletes the tree
 ****************************************/
template <class T>
void deleteBinaryTree(BinaryNode<T>* node) {
  if (node == NULL)
    return;
  if (node->pLeft != NULL)
    deleteBinaryTree(node->pLeft);
  if (node->pRight != NULL)
    deleteBinaryTree(node->pRight);

  delete node;
}

/****************************************************
 * FIND DEPTH
 ****************************************************/
template <class T>
int BinaryNode<T>::findDepth() const {
  if (pRight == NULL && pLeft == NULL)
    return (isRed ? 0 : 1);

  if (pRight != NULL)
    return (isRed ? 0 : 1) + pRight->findDepth();
  else
    return (isRed ? 0 : 1) + pLeft->findDepth();
}

/*************************************************
 * ROTATERIGHT
 * Rotates tree to  right
 ************************************************/
template <class T>
void BinaryNode<T>::rotateRight(BinaryNode<T>* tree) {
  BinaryNode<T>* temp;
  temp = pParent->pRight;

  if (temp->pLeft != NULL)
    pParent->pRight = temp->pLeft;
  else
    pParent->pRight = NULL;

  temp->pLeft = pParent;

  return;
}

/****************************************************
 * BINARY NODE :: VERIFY RED BLACK
 * Do all four red-black rules work here?
 * Author: Br. Helfrich
 ***************************************************/
template <class T>
void BinaryNode<T>::verifyRedBlack(int depth) const {
  depth -= (isRed == false) ? 1 : 0;

  // Rule a) Every node is either red or black
  assert(isRed == true || isRed == false);  // this feels silly

  // Rule b) The root is black
  if (pParent == NULL)
    assert(isRed == false);

  // Rule c) Red nodes have black children
  if (isRed == true) {
    if (pLeft != NULL)
      assert(pLeft->isRed == false);
    if (pRight != NULL)
      assert(pRight->isRed == false);
  }

  // Rule d) Every path from a leaf to the root has the same # of black nodes
  if (pLeft == NULL && pRight && NULL)
    assert(depth == 0);
  if (pLeft != NULL)
    pLeft->verifyRedBlack(depth);
  if (pRight != NULL)
    pRight->verifyRedBlack(depth);
}

/******************************************************
 * VERIFY B TREE
 * Verify that the tree is correctly formed
 * Author: Br. Helfrich
 ******************************************************/
template <class T>
void BinaryNode<T>::verifyBTree() const {
  // check parent
  if (pParent)
    assert(pParent->pLeft == this || pParent->pRight == this);

  // check left
  if (pLeft) {
    assert(pLeft->data <= data);
    assert(pLeft->pParent == this);
    pLeft->verifyBTree();
  }

  // check right
  if (pRight) {
    assert(pRight->data >= data);
    assert(pRight->pParent == this);
    pRight->verifyBTree();
  }
}

/*************************************************
* BinaryNode :: BALANCE
* Calls the right function to balance the tree
************************************************/
template <class T>
void BinaryNode<T>::balance() {
  BinaryNode<T>* pNew = this;
  bool done = false;

  while (!done) {
    if (pParent == NULL) {
      pNew->isRed = false;
      done = true;
    }

    else if (pNew->isRed && pNew->pParent->isRed && pNew->grandParent == NULL) {
      pNew->isRed = false;
      if (pAunt != NULL)
        pNew->pAunt->isRed = false;
      done = true;
    } else if (pNew->isRed && !pNew->pParent->isRed &&
               pNew->grandParent == NULL)
      done = true;

    else if (pNew->pLeft != NULL) {
      pNew = pNew->pLeft;
    } else {
      if (pNew->pRight != NULL)
        pNew = pNew->pRight;
      else {
        done = true;
      }
    }
  }

  return;
}

#endif  // BNODE_H
