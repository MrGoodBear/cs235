/***********************************************************************
 * Module:
 *    Sort Heap
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This program implements the Heap Sort
 ************************************************************************/

#ifndef SORT_HEAP_H
#define SORT_HEAP_H
#include "heap.h"
/*****************************************************
 * SORT HEAP
 * Perform the heap sort
 ****************************************************/
template <class T>
void sortHeap(T array[], int num) {
  T sortedArray[num + 1];
  for (int i = 0; i < num; i++)
    sortedArray[i + 1] = array[i];

  Heap<T>* heap = new Heap<T>(sortedArray, num);    heap->sort(heap->getData(), heap->getNumItems());  // sort the heap
  for (int i = 1; i <= num;
       i++)  // take the sorted array and put it back into the original array
    array[i - 1] = heap->getData()[i];
}

#endif  // SORT_HEAP_H
