/***********************************************************************
 * Module:
 *    Bubble Sort
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This program  implements the Bubble Sort
 ************************************************************************/

#ifndef SORT_BUBBLE_H
#define SORT_BUBBLE_H

/*****************************************************
 * SORT BUBBLE
 * Perform the bubble sort
 ****************************************************/
template <class T>
void sortBubble(T array[], int num) {
  for (int i = 0; i < num - 1; i++) {
    for (int j = num - 1; j >= i + 1; j--) {
      if (array[j - 1] > array[j])  // swaps values
      {
        T temp = array[j];
        array[j] = array[j - 1];
        array[j - 1] = temp;
      }
    }
  }
}

#endif  // SORT_BUBBLE_H
