/***********************************************************************
 * Module:
 *    Week 11, Sort Select
 *    Brother Helfrich, CS 235
 * Author:
 *   Cameron Lewis
 * Summary:
 *    This program will implement the Selection Sort
 ************************************************************************/

#ifndef SORT_SELECTION_H
#define SORT_SELECTION_H

/*****************************************************
 * SORT SELECTION
 * Perform the selection sort
 ****************************************************/
template <class T>
void sortSelection(T array[], int num)
{
   int min;
   for(int x = 0; x < num - 1; x++)  {
		min = x;

		for(int y = x + 1; y < num; y++)   {
   		if(array[min] > array[y])
				min = y;
		}

      if (min != x){
         T temp = array[min];
         array[min] = array[x];
		    array[x] = temp;
      }
   }
}


#endif // SORT_SELECTION_H
