/***********************************************************************
 * Module:
 *    Week 11, Sort Merge
 *    Brother Helfrich, CS 235
 * Author:
 *    <author>
 * Summary:
 *    File implementing the merge sort
 ************************************************************************/

#ifndef SORT_MERGE_H
#define SORT_MERGE_H

#include <iostream>
using namespace std;

template <class T>
void splitMerge(T array[], int iBegin, int iEnd);
template <class T>
void merge(T array[], int iBegin, int iMiddle, int iEnd);

/*****************************************************
 * SORT MERGE
 * Perform the merge sort
 ****************************************************/
template <class T>
void sortMerge(T array[], int num) {
  splitMerge(array, 0, num - 1);
}

/*****************************************************
 * SORT MERGE
 * Perform the merge sort
 ****************************************************/
template <class T>
void splitMerge(T array[], int iBegin, int iEnd) {
  int mid;
  if (iBegin < iEnd) {
    mid = iBegin + (iEnd - iBegin) / 2;

    // Sort first, second piece/halves
    splitMerge(array, iBegin, mid);
    splitMerge(array, mid + 1, iEnd);

    merge(array, iBegin, mid, iEnd);
  }
}

/*****************************************************
 * SORT MERGE
 * Perform the merge sort
 ****************************************************/
template <class T>
void merge(T array[], int iBegin, int iMiddle, int iEnd) {
  int index1, index2, begin;
  int n1 = iMiddle - iBegin + 1;
  int n2 = iEnd - iMiddle;

  T L[n1], R[n2];

  for (index1 = 0; index1 < n1; index1++)
    L[index1] = array[iBegin + index1];

  for (index2 = 0; index2 < n2; index2++)
    R[index2] = array[iMiddle + 1 + index2];

  index1 = 0;
  index2 = 0;
  begin = iBegin;

  while (index1 < n1 && index2 < n2) {
    if (L[index1] <= R[index2]) {
      array[begin] = L[index1];
      index1++;
    } else {
      array[begin] = R[index2];
      index2++;
    }

    begin++;
  }

  while (index1 < n1) {
    array[begin] = L[index1];
    index1++;
    begin++;
  }

  while (index2 < n2) {
    array[begin] = R[index2];
    index2++;
    begin++;
  }
}

template <class T>
bool operator<=(T& lhs, T& rhs) {
  return (rhs > lhs || lhs == rhs);
}

#endif  // SORT_MERGE_H
