/***********************************************************************
 * Module:
 *    Insertion Sort
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This file contains the insertion sort
 ************************************************************************/

#ifndef SORT_INSERTION_H
#define SORT_INSERTION_H

#include <cassert>
using namespace std;

template <class T>
void sortInsertion(T array[], int num) {
  int middle;
  int swapIndx;
  int ceiling, floor;
  T tmpItem;

  for (int i = 1; i < num; i++) {
    floor = 0, ceiling = i;
    middle = i / 2;
    while (floor < ceiling) {
      if (array[middle] > array[i])
        ceiling = middle;
      else if (array[i] > array[middle])
        floor = middle + 1;
      else
        break;
      middle = ((ceiling - floor) / 2) + floor;
    }
    tmpItem = array[i];
    for (swapIndx = i - 1; swapIndx >= middle; swapIndx--)
      array[swapIndx + 1] = array[swapIndx];
    array[middle] = tmpItem;
  }
}

#endif  // SORT_INSERTION_H
