/***********************************************************************
 * Module:
 *    Heap
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This program implements a stack
 ************************************************************************/

#ifndef WEEK11_HEAP_H
#define WEEK11_HEAP_H
using namespace std;

template <class T>
class Heap {
 public:
  // default constuctor
  Heap() : data(NULL), numItems(0){};

  // copy constructor
  Heap(T array[], int num) {
    numItems = num;
    data = new T[num + 1];
    for (int i = 0; i <= numItems; i++) {
      data[i] = array[i];
    }
    heapify(data, numItems);
  }

  // deletes max value
  void deleteMax() {
    data[1] = data[numItems];
    numItems--;
    percolateDown(data, 1, numItems);
  }

  void percolateDown(T* a, int i, int n) {
    int j;
    T temp = a[i];
    j = 2 * i;
    while (j <= n) {
      if (j < n && a[j + 1] > a[j])
        j = j + 1;

      if (a[j] > temp || a[j] == temp) {
        a[j / 2] = a[j];
        j = 2 * j;
      } else if (temp > a[j])
        break;
    }
    a[j / 2] = temp;
    return;
  }

  void sort(T* a, int n) {
    int i;
    T temp;
    for (i = n; i >= 2; i--) {
      temp = a[i];
      a[i] = a[1];
      a[1] = temp;
      percolateDown(a, 1, i - 1);
    }
  }

  // Turns a given array into a heap.
  void heapify(T* a, int n) {
    int i;
    for (i = n / 2; i >= 1; i--) {
      percolateDown(a, i, n);
    }
  }

  // getters
  T* getData() { return data; };
  int getNumItems() { return numItems; }
  T getMax() { return data[1]; }

  // destructor
  ~Heap() { delete[] data; }

 private:
  T* data;
  int numItems;
};

#endif  // WEEK11_HEAP_H
