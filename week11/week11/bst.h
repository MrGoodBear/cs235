/***********************************************************************
 * Component:
 *    Week 09, Binary Search Tree (BST)
 *    Brother JonesL, CS 235
 * Author:
 *    Br. Helfrich
 *    Modified by Scott Ercanbrack - removed most of the the BST class
 *                                   functions, but left BST Iterator class
 *
 *
 * Summary:
 *    Creates a binary search tree
 ************************************************************************/

#ifndef BST_H
#define BST_H

#include "bnode.h"  // for BinaryNode
#include "stack.h"  // for Stack
#include <cassert>

// forward declaration for the BST iterator
template <class T>
class BSTIterator;

/*****************************************************************
 * BINARY SEARCH TREE
 * Create a Binary Search Tree
 *****************************************************************/
template <class T>
class BST {
 public:
  // constructor
  BST() : root(NULL){};

  // copy constructor
  BST(const BST<T>& rhs);

  ~BST();

  // returns the size of the tree
  int size() { return sizeAux(root); }

  // determine if the tree is empty
  bool empty() const { return (root == 0); }

  // clear all the contests of the tree
  void clear() {
    deleteBinaryTree(root);
    root = NULL;
  }

  // overloaded assignment operator
  BST<T>& operator=(const BST<T>& rhs) throw(const char*) {
    if (this != &rhs) {
      if (root != NULL)
        deleteBinaryTree(root);
      if (rhs.root == NULL)
        root = NULL;
      else
        copyTree(this->root, rhs.root);
    }
    return *this;
  }

  // insert an item
  void insert(const T& item) throw(const char*);

  // remove an item
  void remove(BSTIterator<T>& it);

  // find a given item
  BSTIterator<T> find(const T& t);

  // the usual iterator stuff
  BSTIterator<T> begin();
  BSTIterator<T> end() { return BSTIterator<T>(NULL); }
  BSTIterator<T> rbegin();
  BSTIterator<T> rend() { return BSTIterator<T>(NULL); }

 private:
  BinaryNode<T>* root;

  // calculates the size of the tree
  int sizeAux(BinaryNode<T>* node) {
    if (node == NULL)
      return (0);
    else
      return (sizeAux(node->pLeft) + 1 + sizeAux(node->pRight));
  }

  void copyTree(BinaryNode<T>*& copiedNode, BinaryNode<T>* node) {
    if (node == NULL)
      copiedNode = NULL;
    else {
      copiedNode = new BinaryNode<T>;
      copiedNode->data = node->data;
      copyTree(copiedNode->pLeft, node->pLeft);
      copyTree(copiedNode->pRight, node->pRight);
    }
  }

  //   void insertAux(BinaryNode<T> *& node, const T & item) {
  //      if (node == 0)
  //         node = new BinaryNode<T>(item);
  //      else if (item < node->data)
  //         insertAux(node->pLeft, item);
  //      else
  //         insertAux(node->pRight, item);
  //   }
};

/*********************************************************
* copy constructor
**********************************************************/
template <class T>
BST<T>::BST(const BST<T>& rhs) {
  copyTree(this->root, rhs.root);
}

/*****************************************************
* Destructor
*******************************************************/
template <class T>
BST<T>::~BST() {
  deleteBinaryTree(root);
}

/*****************************************************
 * BST :: BEGIN
 * Return the first node (left-most) in a binary search tree
 ****************************************************/
template <class T>
BSTIterator<T> BST<T>::begin() {
  Stack<BinaryNode<T>*> nodes;

  nodes.push(NULL);
  nodes.push(root);
  while (nodes.top() != NULL && nodes.top()->pLeft)
    nodes.push(nodes.top()->pLeft);

  return nodes;
}

/*****************************************************
 * BST :: RBEGIN
 * Return the last node (right-most) in a binary search tree
 ****************************************************/
template <class T>
BSTIterator<T> BST<T>::rbegin() {
  Stack<BinaryNode<T>*> nodes;

  nodes.push(NULL);
  nodes.push(root);
  while (nodes.top() != NULL && nodes.top()->pRight)
    nodes.push(nodes.top()->pRight);

  return nodes;
}

/*****************************************************
 * BST :: INSERT
 * Insert a node at a given location in the tree
 ****************************************************/
template <class T>
void BST<T>::insert(const T& item) throw(const char*) {
  try {
    BinaryNode<T>* locptr = root;
    BinaryNode<T>* parent = 0;

    while (locptr != NULL) {
      parent = locptr;
      if (locptr->data > item)
        locptr = locptr->pLeft;
      else
        locptr = locptr->pRight;
    }

    locptr = new BinaryNode<T>(item);
    if (parent == NULL)
      root = locptr;
    else if (parent->data > item) {
      parent->pLeft = locptr;
      locptr->pParent = parent;
    } else {
      parent->pRight = locptr;
      locptr->pParent = parent;
    }

  } catch (...) {
    throw "ERROR: Unable to allocate a node";
  }
}

/*************************************************
 * BST :: REMOVE
 * Remove a given node as specified by the iterator
 ************************************************/
template <class T>
void BST<T>::remove(BSTIterator<T>& it) {
  BinaryNode<T>* x = it.getNode();
  BinaryNode<T>* parent = x->pParent;

  if (x->pLeft != 0 && x->pRight != 0) {
    BinaryNode<T>* xSucc = x->pRight;
    parent = x;

    while (xSucc->pLeft != 0) {
      parent = xSucc;
      xSucc = xSucc->pLeft;
    }
    x->data = xSucc->data;
    x = xSucc;
  }

  BinaryNode<T>* subTree = x->pLeft;
  if (subTree == 0)
    subTree = x->pRight;
  if (parent == 0)
    root = subTree;
  else if (parent->pLeft == x)
    parent->pLeft = subTree;
  else
    parent->pRight = subTree;

  delete x;
}

/****************************************************
 * BST :: FIND
 * Return the node corresponding to a given value
 ****************************************************/
template <class T>
BSTIterator<T> BST<T>::find(const T& t) {
  BinaryNode<T>* pCurrNode = root;
  bool dataFound = false;
  while (!dataFound && pCurrNode != NULL) {
    if (t < pCurrNode->data)
      pCurrNode = pCurrNode->pLeft;
    else if (pCurrNode->data < t)
      pCurrNode = pCurrNode->pRight;
    else {
      dataFound = true;
    }
  }
  if (dataFound)
    return pCurrNode;
  else
    return end();
}

/**********************************************************
 * BINARY SEARCH TREE ITERATOR
 * Forward and reverse iterator through a BST
 *********************************************************/
template <class T>
class BSTIterator {
 public:
  // constructors
  BSTIterator(BinaryNode<T>* p = NULL) { nodes.push(p); }
  BSTIterator(Stack<BinaryNode<T>*>& s) { nodes = s; }
  BSTIterator(const BSTIterator<T>& rhs) { nodes = rhs.nodes; }

  // assignment
  BSTIterator<T>& operator=(const BSTIterator<T>& rhs) {
    // need an assignment operator for the Stack class.
    nodes = rhs.nodes;
    return *this;
  }

  // compare
  bool operator==(const BSTIterator<T>& rhs) const {
    // only need to compare the leaf node
    return rhs.nodes.const_top() == nodes.const_top();
  }
  bool operator!=(const BSTIterator<T>& rhs) const {
    // only need to compare the leaf node
    return rhs.nodes.const_top() != nodes.const_top();
  }

  // de-reference. Cannot change because it will invalidate the BST
  T& operator*() { return nodes.top()->data; }

  // iterators
  BSTIterator<T>& operator++();
  BSTIterator<T> operator++(int postfix) {
    BSTIterator<T> itReturn = *this;
    ++(*this);
    return itReturn;
  }
  BSTIterator<T>& operator--();
  BSTIterator<T> operator--(int postfix) {
    BSTIterator<T> itReturn = *this;
    --(*this);
    return itReturn;
  }

  // must give friend status to remove so it can call getNode() from it
  friend void BST<T>::remove(BSTIterator<T>& it);
  // get the node pointer

 private:
  BinaryNode<T>* getNode() { return nodes.top(); }

  // the stack of nodes
  Stack<BinaryNode<T>*> nodes;
};

/**************************************************
 * BST ITERATOR :: INCREMENT PREFIX
 * advance by one
 *************************************************/
template <class T>
BSTIterator<T>& BSTIterator<T>::operator++() {
  // do nothing if we have nothing
  if (nodes.top() == NULL)
    return *this;

  // if there is a right node, take it
  if (nodes.top()->pRight != NULL) {
    nodes.push(nodes.top()->pRight);

    // there might be more left-most children
    while (nodes.top()->pLeft)
      nodes.push(nodes.top()->pLeft);
    return *this;
  }

  // there are no right children, the left are done
  assert(nodes.top()->pRight == NULL);
  BinaryNode<T>* pSave = nodes.top();
  nodes.pop();

  // if the parent is the NULL, we are done!
  if (NULL == nodes.top())
    return *this;

  // if we are the left-child, got to the parent.
  if (pSave == nodes.top()->pLeft)
    return *this;

  // we are the right-child, go up as long as we are the right child!
  while (nodes.top() != NULL && pSave == nodes.top()->pRight) {
    pSave = nodes.top();
    nodes.pop();
  }

  return *this;
}

/**************************************************
 * BST ITERATOR :: DECREMENT PREFIX
 * advance by one
 *************************************************/
template <class T>
BSTIterator<T>& BSTIterator<T>::operator--() {
  // do nothing if we have nothing
  if (nodes.top() == NULL)
    return *this;

  // if there is a left node, take it
  if (nodes.top()->pLeft != NULL) {
    nodes.push(nodes.top()->pLeft);

    // there might be more right-most children
    while (nodes.top()->pRight)
      nodes.push(nodes.top()->pRight);
    return *this;
  }

  // there are no left children, the right are done
  assert(nodes.top()->pLeft == NULL);
  BinaryNode<T>* pSave = nodes.top();
  nodes.pop();

  // if the parent is the NULL, we are done!
  if (NULL == nodes.top())
    return *this;

  // if we are the right-child, got to the parent.
  if (pSave == nodes.top()->pRight)
    return *this;

  // we are the left-child, go up as long as we are the left child!
  while (nodes.top() != NULL && pSave == nodes.top()->pLeft) {
    pSave = nodes.top();
    nodes.pop();
  }

  return *this;
}

#endif  // BST_H
