/***********************************************************************
 * Implementation:
 *    stock
 * Summary:
 *    Contains the implementation for stocksBuySell() as well
 *    as the implementation of the Stock class.
 * Author:
 *    Cameron Lewis
 **********************************************************************/

#include <iostream>    // for ISTREAM, OSTREAM, CIN, and COUT
#include <string>      // for STRING
#include <cassert>     // for ASSERT
#include "stock.h"     // for stock_stock
#include "queue.h"     // for QUEUE
using namespace std;

/************************************************
 * stocks BUY SELL
 * The interactive function allowing the user to
 * buy and sell stocks
 ***********************************************/
void stocksBuySell() {
    // instructions
    cout << "This program will allow you to buy and sell stocks. "
         << "The actions are:\n";
    cout << "  buy 200 $1.57   - Buy 200 shares at $1.57\n";
    cout << "  sell 150 $2.15  - Sell 150 shares at $2.15\n";
    cout << "  display         - Display your current stock portfolio\n";
    cout << "  quit            - Display a final report and quit the program\n";

    // creating Stock object from class
    Stock stock;

    // variables to help us parse user input
    int input = 0;
    string command;
    int desiredQuantity = 0;
    Dollars desiredPrice;


    // loop to capture user input and send it to the right place
    do {
        while (input == 0) {
            cout << "> ";
            cin >> command;
            if (command == "buy") {
                input = 1;
            }
            else if (command == "sell") {
                input = 2;
            }
            else if (command == "display") {
                input = 3;
                break;
            }
            else if (command == "quit") {
                input = 4;
                break;
            }

            cin >> desiredQuantity;
            cin >> desiredPrice;
        }

        switch (input) {
            // buy stock
            case 1: {
                stock.buy(desiredQuantity, desiredPrice);
                input = 0;
                break;
            }

            // sell stock
            case 2: {
                stock.sell(desiredQuantity, desiredPrice);
                input = 0;
                break;
            }

            // display portfolio
            case 3: {
                stock.display();
                input = 0;
                break;
            }

            // quits program
            case 4: {
                break;
            }
        }
    } while (input != 4);
}

/************************************************
 * FUNCTION--DISPLAY
 *
 * Shows us all the information we want to know
 * about our sold and purchased stocks, such as
 * how much we purchased and sold, at what price,
 * and the profits obtained from their sale.
 ***********************************************/
void Stock::display() {
    
    // put everything into temporary queues so they don't disappear forever after pop()
    temp_Batch = Batch;
    temp_buyPrices = buyPrices;
    temp_soldBatch = soldBatch;
    temp_sellingPrices = sellingPrices;
    temp_Profits = Profits;
    totalProfit = 0;
    Dollars dollars;

    // while there's still held stocks to display, display them
    if (!temp_Batch.empty()) {
        cout << "Currently held:" << endl;
    }

    while (!temp_Batch.empty()) {
        dollars = temp_buyPrices.front();

        cout << "\tBought " << temp_Batch.front() << " shares at " << dollars << endl;
        temp_buyPrices.pop();
        temp_Batch.pop();
    }

    // displays all sold stock IF stock has been sold at some point
    if (sold == true) {
        Dollars sellingPrice;
        Dollars saleProfit;

        cout << "Sell History:\n";

        while (!temp_soldBatch.empty()) {
            sellingPrice = temp_sellingPrices.front();
            saleProfit = temp_Profits.front();

            totalProfit += temp_Profits.front();

            cout << "\tSold " << temp_soldBatch.front() << " shares at "
            << sellingPrice << " for a profit of " << saleProfit << endl;
            temp_soldBatch.pop();
            temp_Profits.pop();

            int x = temp_soldBatch.size();
            int y = temp_sellingPrices.size();

            if ((y - 1 == x && y > 1) || (x == y && y > 1)) {
                temp_sellingPrices.pop();
            }
        }
    }
    cout << "Proceeds: " << totalProfit << endl;
}

/************************************************
 * FUNCTION--BUY
 *
 * A function responsible for recording and
 * storing information bout purchased stocks.
 ***********************************************/
void Stock::buy(int quantity, Dollars cents) {
    Batch.push(quantity);
    buyPrices.push(cents);
};

/************************************************
 * FUNCTION--SELL
 *
 * A function responsible for recording and
 * storing information bout sold stocks. Also
 * makes sure the partial and complete sale
 * of held stock batches are recorded correctly.
 ***********************************************/
void Stock::sell(int quantity, Dollars sellingPrice) {
    sold = true;
    sellingPrices.push(sellingPrice);
    int currentBatch = Batch.front();


    // keeps looping until every stock has been successfully sold and accounted for
    while (quantity != 0) {
        if (quantity >= currentBatch) {
            quantity = quantity - currentBatch;
            soldBatch.push(currentBatch);
            Dollars stuff = (sellingPrice - buyPrices.front());
            Profits.push(stuff * currentBatch);
            Batch.pop();
            buyPrices.pop();
            if (!Batch.empty()) {
                currentBatch = Batch.front();
            }
        }
        else if (quantity < currentBatch) {
            currentBatch = currentBatch - quantity;
            soldBatch.push(quantity);
            Dollars stuff = (sellingPrice - buyPrices.front());
            Profits.push(stuff * quantity);
            quantity = 0;

            // this handy loop copies and reassigns amount of held stock
            // when a partial batch sale happens

            Queue<int> temp;
            for (int i = 0; i < Batch.size(); i++) {
                if (i == 0) {
                    temp.push(currentBatch);
                }
                else {
                    temp.push(Batch.front());
                }
                Batch.pop();
            }
            Batch = temp;
        }
    }
};
