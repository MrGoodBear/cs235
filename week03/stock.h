/***********************************************************************
 * Header:
 *    STOCK
 * Summary:
 *    Contains class definition for the Stock class.
 * Author
 *    Cameron Lewis
 ************************************************************************/

#ifndef STOCK_H
#define STOCK_H

#include "dollars.h"   // for Dollars defined in StockTransaction
#include "queue.h"     // for QUEUE
#include <iostream>    // for ISTREAM and OSTREAM

// the interactive stock buy/sell function
void stocksBuySell();

/************************************************
 * CLASS-- Stock
 *
 * Holds everything there is to hold as it relates to
 * buying, selling, and displaying held and sold stock.
 * Based on the inherently prioritizing nature of Queues,
 * the container ADL at use here in storing the required
 * information.
 ***********************************************/
class Stock {
private:
    
    // stores all the prices at which stocks were purchased
    Queue<Dollars> buyPrices;
    Queue<Dollars> temp_buyPrices;
    
    // stores all prices at which stocks were sold
    Queue <Dollars> sellingPrices;
    Queue <Dollars> temp_sellingPrices;
    
    // holds all sold stock batches
    Queue <int> soldBatch;
    Queue <int> temp_soldBatch;
    
    // stores all stock batches still being held
    Queue<int> Batch;
    Queue<int> temp_Batch;
    
    // stores profit
    Queue<Dollars> Profits;
    Queue<Dollars> temp_Profits;
    
    // running total of all profits
    Dollars totalProfit;
    
    // helps us when its time to display sold stocks
    bool sold;
    
public:
    // simple constructor. Everything else uses its own constructor
    Stock() : totalProfit(0), sold(false) {};
    
    // displays everything we want to know about our stock
    void display();
    
    // records and stores info about stock purchases
    void buy(int quantity, Dollars cents);
    
    // records & stores info about stock sales and
    // redistributes batches during partial sales
    void sell(int quantity, Dollars sellingPrice);
 };
#endif // STOCK_H

