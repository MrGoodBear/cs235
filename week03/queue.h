#ifndef CONTAINER_H
#define CONTAINER_H
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <new>
#include <stdexcept>
#include <string>
using namespace std;

/************************************************
 * Queue
 * A container class that holds stuff in specific order
 ***********************************************/
template <typename T> class Queue {
public:
    // default constructor : empty and kinda useless
    Queue() : maxItems(0), numItems(0),  myFront(0), myBack(0), data(new T[0]) {
    };

    // copy constructor : copy it
    Queue(const Queue<T> &rhs);

    // non-default constructor : pre-allocate
    Queue(int maxItems) throw(const char *);

    // destructor : free everything
    ~Queue() {
        if (maxItems) {
            data = NULL;
        }
    }

    void push(const T &t) throw(const char *);

    bool empty() const;

    void pop() throw(const char *);

    const T &front() const;

    void findAge();

    const T &back();

    // remove all the items from the container
    void clear() {
        numItems = 0;
    }

    // how many items are currently in the container?
    int size() const {
        return numItems;
    }

    // returns the current capacity of the Queue
    int capacity() const {
        return maxItems;
    }

    // overloaded assignment operator
    Queue<T> operator = (const Queue rhs) {
        // do nothing if there is nothing to do
        if (rhs.numItems == 0) {
            maxItems = numItems = 0;
            data = NULL;
        }

        if (numItems != rhs.numItems) {
            // attempt to allocate
            maxItems = rhs.maxItems;
            numItems = rhs.numItems;
            myFront = rhs.myFront;
            myBack = rhs.myBack;
            maxItems = rhs.maxItems;
            age = rhs.age;
            try {
                data = new T[maxItems];
            } catch(std::bad_alloc) {
                throw "ERROR: Unable to allocate buffer";
            }
        }

        // copy the items over one at a time using the default assignment operator
        for (int i = 0; i < maxItems; i++) {
            data[i] = rhs.data[i];
        }

        return *this;
    }

    // overloaded call operator
    Queue<T> operator() (int a, int b);

private:
    int numItems; // how many items are currently in the Queue?
    int maxItems; // how many items can I put on the Queue before full?
    int myBack;
    int myFront;
    T * data; // dynamically allocated array of T
    int age;
};

/*******************************************
* CONTAINER::COPY CONSTRUCTOR
*******************************************/
template <class T>
Queue <T>::Queue(const Queue<T> &rhs) {
    //assert(rhs.maxItems >= 0);

    // do nothing if there is nothing to do
    if (rhs.maxItems == 0) {
        maxItems = numItems = 0;
        data = NULL;
        myFront = myBack = 0;
        return;
    }

    this->maxItems = rhs.maxItems;
    this->numItems = rhs.numItems;
    this->myFront = rhs.myFront;
    this->myBack = rhs.myBack;
    //data = new T;
    try {
        if (!empty()) {
            data = new T[maxItems];

            for (int i = 0; i < maxItems; i++) {
                data[i] = rhs.data[i];
            }
        }
    } catch(std::bad_alloc) {
        throw "ERROR: Unable to allocate a new buffer for Queue";
    }
}
/**********************************************
* CONTAINER : NON-DEFAULT CONSTRUCTOR
* Preallocate the container to "maxItems"
**********************************************/
template <typename T>
Queue<T>::Queue(int maxItems) throw(const char *) {
    assert(maxItems >= 0);

    // do nothing if there is nothing to do
    if (maxItems == 0) {
        this->maxItems = this->numItems = 0;
        this->data = NULL;
        return;
    }

    // attempt to allocate
    try {
        data = new T[maxItems];
    } catch(std::bad_alloc) {
        throw "ERROR: Unable to allocate a new buffer for Stack";
    }

    // copy over the stuff
    this->maxItems = maxItems;
    this->numItems = 0;
    this->myBack = 0;
    this->myFront = 0;

    // initialize the container by calling the default constructor
    for (int i = 0; i < maxItems; i++) {
        data[i] = T();
    }
}
/**********************************************
* CONTAINER : PUSH
* allows new values to be placed in Queue
**********************************************/
template <typename T>
void Queue<T>::push(const T &t) throw(const char *) {
    int newCap = 0;

    int newBack = myBack + 1;

    if (maxItems > 0) {
        newBack = (myBack + 1) % maxItems;
    }

    // check if empty or full (still maintaining a blank space)
    // then update capacity
    if (maxItems == 0 || abs(size() - maxItems) == 0) {
        if (maxItems == 0) {
            newCap = 4;     // default to 4
        }
        else {
            newCap = (maxItems * 2);
        }

        // copy data into temp
        T * tempData = new T[maxItems];

        for (int i = 0; i < size(); i++) {
            int index = (i + myFront) % size();
            tempData[i] = data[index];
        }

        // attempt to allocate
        try {
            data = NULL;
            delete[] data;
            data = new T[newCap + 1];
            maxItems = newCap;

            // after reallocating space, correct myBack and myFront
            myBack = numItems;
            newBack = (myBack + 1) % maxItems;
            myFront = 0;

            // copy tempData into newly allocated data
            int i;
            for (i = 0; i < numItems; i++) {
                data[i] = tempData[i];
            }

            delete[] tempData;     // free memory allocated for temp data
            tempData = tempData - NULL;       // prevent deallocated memory from being used.
        } catch(std::bad_alloc) {
            throw "ERROR: Unable to allocate a new buffer for queue";
        }
    }

    // add new value at end
    data[myBack] = t;
    myBack = newBack;
    numItems++;

    return;
}
/**********************************************
* CONTAINER : EMPTY
* checks to see if Queue object is empty
**********************************************/
template <typename T>
bool Queue<T>::empty() const {
    if (numItems <= 0) {
        return true;
    }
    else {
        return false;
    }
}
/**********************************************
* CONTAINER : POP
* removes items from front of Queue
**********************************************/
template <typename T>
void Queue<T>::pop() throw(const char *) {
    if (empty()) {
        throw "ERROR: attempting to pop from an empty queue";
    }
    else {
        myFront = (myFront + 1) % maxItems;
        numItems--;
    }
}

/**********************************************
* CONTAINER : FRONT
* returns item in front of queue
**********************************************/
template <typename T>
const T &Queue<T>::front() const {
    if (empty()) {
        throw "ERROR: attempting to access an item in an empty queue";
    }
    else {
        return data[myFront];
    }
}

/**********************************************
* CONTAINER : BACK
* returns item in back of queue
**********************************************/
template <typename T>
const T &Queue<T>::back() {
    if (empty()) {
        throw "ERROR: attempting to access an item in an empty queue";
    }
    else {
        return data[myBack];
    }
}

#endif // CONTAINER_H
