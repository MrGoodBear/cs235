/***********************************************************************
 * Module:
 *    Week 02, Stack
 *    Brother JonesL, CS 235
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This program will implement the testInfixToPostfix()
 *    and testInfixToAssembly() functions
 ************************************************************************/

#include "infix.h"
#include "stack.h"  // for STACK
#include <cassert>  // for ASSERT
#include <iostream> // for ISTREAM and COUT
#include <string>   // for STRING
using namespace std;

// This function determines the precedence of each operator and
// returns it to compareOperator()
int precedence(const char &op) {
    int priority = 0;
    
    if (op == '^')
        priority = 3;
    if (op == '*' || op == '/')
        priority = 2;
    if (op == '+' || op == '-')
        priority = 1;
    
    return priority;
}
// This function compares the operators to determine how the operators should be
// placed within the
// postfix string
void compareOperator(const char &op, Stack<char> &opStack, string &postfix) {
    
    if (opStack.empty()) {
        opStack.push(op);
    } else {
        
        // if precedence of current operator is greater than the precedence of
        // opStack.top()... push it onto the stack
        if (op != '(' && op != ')') {
            if (precedence(op) > precedence(opStack.top())) {
                if (opStack.top() != '(') {
                    postfix.append(space + opStack.top());
                }
                opStack.pop();
                opStack.push(op);
            }
        }
        
        else if (op == '(') {
            opStack.push(op);
        } else if (op == ')') {
            StackIterator<char> it;
            for (it = opStack.begin(); *it == '('; ++it) {
                if (*it != '(' || *it != ')') {
                    postfix.append(space + opStack.top());
                    opStack.pop();
                }
            }
        }
        // keep
        else {
            while (!opStack.empty() && precedence(op) <= precedence(opStack.top())) {
                postfix.append(space + opStack.top());
                opStack.pop();
            }
            opStack.push(op);
        }
    }
}

/*****************************************************
 * CONVERT INFIX TO POSTFIX
 * Convert infix equation "5 + 2" into postifx "5 2 +"
 *****************************************************/
string convertInfixToPostfix(const string &infix) {
    string postfix;
    Stack<char> opStack;
    // StackIterator<string> it;
    // for (it = infix.begin(); it != infix.empty(); ++it)
    //    cout << "  " << i++ << "\t" << *it << endl;
    char token = '\0';
    
    // keep looping until infix equation is empty
    for (int i = 0; i < infix.length(); i++) {
        const char lastToken = token;
        token = infix[i];
        // if last token was not a space, and this token is not a space, then....
        
        // if operand, append to postfix
        if ((isalnum(token) && token != ' ') || token == '.') {
            if (lastToken != ' ' && lastToken != '\0') {
                postfix.push_back(token);
            } else
                postfix.append(space + token);
        }
        
        // if operator, send to be compared
        else if (!isalnum(token) && token != ' ') {
            compareOperator(token, opStack, postfix);
        }
    }
    // keep popping remaining operands to postfix until empty
    while (!opStack.empty()) {
        postfix.append(space + opStack.top());
        opStack.pop();
    }
    
    /*
     if (!isalnum(token)) {
     operator_stack.push(token);
     cout << operator_stack.top();
     */
    
    return postfix;
}

/*****************************************************
 * TEST INFIX TO POSTFIX
 * Prompt the user for infix text and display the
 * equivalent postfix expression
 *****************************************************/
void testInfixToPostfix() {
    string input;
    cout << "Enter an infix equation.  Type \"quit\" when done.\n";
    
    do {
        // handle errors
        if (cin.fail()) {
            cin.clear();
            cin.ignore(256, '\n');
        }
        
        // prompt for infix
        cout << "infix > ";
        getline(cin, input);
        
        // generate postfix
        if (input != "quit") {
            string postfix = convertInfixToPostfix(input);
            cout << "\tpostfix: " << postfix << endl << endl;
        }
    } while (input != "quit");
}

/**********************************************
 * CONVERT POSTFIX TO ASSEMBLY
 * Convert postfix "5 2 +" to assembly:
 *     LOAD 5
 *     ADD 2
 *     STORE VALUE1
 **********************************************/
string convertPostfixToAssembly(const string &postfix) {
    string assembly;
    
    return assembly;
}

/*****************************************************
 * TEST INFIX TO ASSEMBLY
 * Prompt the user for infix text and display the
 * resulting assembly instructions
 *****************************************************/
void testInfixToAssembly() {
    string input;
    cout << "Enter an infix equation.  Type \"quit\" when done.\n";
    
    do {
        // handle errors
        if (cin.fail()) {
            cin.clear();
            cin.ignore(256, '\n');
        }
        
        // prompt for infix
        cout << "infix > ";
        getline(cin, input);
        
        // generate postfix
        if (input != "quit") {
            string postfix = convertInfixToPostfix(input);
            cout << convertPostfixToAssembly(postfix);
        }
    } while (input != "quit");
}
