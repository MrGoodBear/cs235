#ifndef CONTAINER_H
#define CONTAINER_H
#include <cassert>
#include <cstddef>
#include <iostream>
#include <new>
#include <stdexcept>
#include <string>
using namespace std;

// forward declaration for StackIterator
template <typename T> class StackIterator;

/************************************************
 * Stack
 * A container class that holds stuff
 ***********************************************/
template <typename T> class Stack {
public:
    // default constructor : empty and kinda useless
    Stack() : numItems(0), maxItems(0), myTop(-1), data(new T) {}
    
    // copy constructor : copy it
    Stack(const Stack &rhs);
    
    // non-default constructor : pre-allocate
    Stack(int maxItems) throw(const char *);
    
    // destructor : free everything
    ~Stack() {
        if (maxItems)
            data = NULL;
    }
    
    void push(const T &value) {
        
        // if capacity is empty, set capacity to 1
        if (maxItems == 0) {
            ++maxItems;
        }
        
        // if capacity is full, double capacity and reallocate
        if (maxItems == numItems) {
            Stack<T> new_data(1);
            try {
                new_data.maxItems = maxItems;
                new_data.data = new T[(maxItems)];
                new_data.numItems = numItems;
                
                for (int i = 0; i < numItems; i++) {
                    new_data.data[i] = data[i];
                }
                
                data = new (std::nothrow) T[(maxItems *= 2)];
                
                for (int i = 0; i < numItems; i++) {
                    data[i] = new_data.data[i];
                }
            } catch (std::bad_alloc) {
                throw "ERROR: Unable to allocate a new buffer for Stack";
            }
        }
        
        // add item to Stack and increase counter
        data[numItems] = value;
        numItems++;
        myTop = numItems - 1;
    }
    
    bool empty() const {
        if (myTop + 1 <= 0)
            return true;
        else
            return false;
    }
    // POP FUNCTION
    void pop() {
        if (empty()) {
            throw "ERROR: Unable to pop from an empty Stack";
        } else
            myTop--;
        numItems--;
        maxItems--;
    }
    
    const T &top();
    
    // remove all the items from the container
    void clear() { numItems = 0; }
    
    // how many items are currently in the container?
    int size() const { return numItems; }
    
    // returns the current capacity of the Stack
    int capacity() { return maxItems; }
    
    // return an iterator to the beginning of the list
    StackIterator<T> begin() { return StackIterator<T>(data); }
    
    // return an iterator to the end of the list
    StackIterator<T> end() { return StackIterator<T>(data + numItems); }
    
    // overloaded subscript operator. Returns by-reference
    T &operator[](int index) {
        return data[index];
        if (index > numItems) {
            throw "ERROR: Invalid index";
        }
        return data[index];
    }
    
    // overloaded subscript operator. Const, and returns const-by-reference
    const T &operator[](int index) const {
        return data[index];
        if (index > numItems) {
            throw "ERROR: Invalid index";
        }
        return data[index];
    }
    
    // overloaded assignment operator
    Stack<T> operator=(const Stack rhs) {
        
        // do nothing if there is nothing to do
        if (rhs.numItems == 0) {
            maxItems = numItems = 0;
            data = NULL;
        }
        
        if (numItems != rhs.maxItems) {
            // attempt to allocate
            maxItems = rhs.maxItems;
            numItems = rhs.numItems;
            myTop = rhs.myTop;
            try {
                data = new T[maxItems];
            } catch (std::bad_alloc) {
                throw "ERROR: Unable to allocate buffer";
            }
        }
        
        // copy the items over one at a time using the default assignment operator
        for (int i = 0; i < numItems; i++)
            data[i] = rhs.data[i];
        
        return *this;
    }
    
private:
    int numItems; // how many items are currently in the Stack?
    int maxItems; // how many items can I put on the Stack before full?
    int myTop;
    T *data; // dynamically allocated array of T
};

/**************************************************
 * CONTAINER ITERATOR
 * An iterator through Stack
 *************************************************/
template <class T> class StackIterator {
public:
    // default constructor
    StackIterator() : p(NULL) {}
    
    // initialize to direct p to some item
    StackIterator(T *p) : p(p) {}
    
    // copy constructor
    StackIterator(const StackIterator &rhs) { *this = rhs; }
    
    // assignment operator
    StackIterator &operator=(const StackIterator &rhs) {
        this->p = rhs.p;
        return *this;
    }
    
    // not equals operator
    bool operator!=(const StackIterator &rhs) const { return rhs.p != this->p; }
    
    // dereference operator
    T &operator*() { return *p; }
    
    // prefix increment
    StackIterator<T> &operator++() {
        p++;
        return *this;
    }
    
    // postfix increment
    StackIterator<T> operator++(int postfix) {
        StackIterator tmp(*this);
        p++;
        return tmp;
    }
    
private:
    T *p;
};

/// TOP
template <class T> const T &Stack<T>::top() {
    if (empty()) {
        throw "ERROR: Unable to reference the element from an empty Stack";
    } else
        return data[myTop];
}

/*******************************************
 * CONTAINER :: COPY CONSTRUCTOR
 *******************************************/
template <class T> Stack<T>::Stack(const Stack<T> &rhs) {
    maxItems = rhs.maxItems;
    numItems = rhs.numItems;
    myTop = rhs.myTop;
    data = new T;
    
    try {
        if (data != 0) {
            data = new T[numItems];
            for (int pos = 0; pos <= myTop; pos++)
                data[pos] = rhs.data[pos];
        }
    } catch (std::bad_alloc) {
        throw "ERROR: Unable to allocate a new buffer for Stack";
    }
}

/**********************************************
 * CONTAINER : NON-DEFAULT CONSTRUCTOR
 * Preallocate the container to "maxItems"
 **********************************************/
template <class T> Stack<T>::Stack(int maxItems) throw(const char *) {
    assert(maxItems >= 0);
    
    // do nothing if there is nothing to do
    if (maxItems == 0) {
        this->maxItems = this->numItems = 0;
        this->data = NULL;
        return;
    }
    
    // attempt to allocate
    try {
        data = new T[maxItems];
    } catch (std::bad_alloc) {
        throw "ERROR: Unable to allocate a new buffer for Stack";
    }
    
    // copy over the stuff
    this->maxItems = maxItems;
    this->numItems = 0;
    
    // initialize the container by calling the default constructor
    for (int i = 0; i < maxItems; i++)
        data[i] = T();
        }

#endif // CONTAINER_H
