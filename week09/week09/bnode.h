/***********************************************************************
 * Header:
 *    BNODE
 * Summary:
 *    Class BNODE
 * Author
 *    Cameron Lewis
 ************************************************************************/

#include <iostream>
#include <string>
using namespace std;

#ifndef WEEK08_BNODE_H
#define WEEK08_BNODE_H

template <class T>
class BinaryNode {
 public:
  // default constructor
  BinaryNode() throw(const char*) {
    try {
      pLeft = NULL;
      pRight = NULL;
      pParent = NULL;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  };

  // copy constructor
  BinaryNode(const BinaryNode<T>& rhs) throw(const char*) {
    try {
      this->pRight = NULL;
      this->pLeft = NULL;
      this->pParent = NULL;
      *this = rhs;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  }

  // non-default constructor
  BinaryNode(T rhs) throw(const char*) {
    try {
      pLeft = NULL;
      pRight = NULL;
      pParent = NULL;
      data = rhs;
    } catch (...) {
      throw "ERROR: unable to allocate a new node for a list";
    }
  };

  // gives us tree size
  int getSize(BinaryNode<T>* node) {
    if (node == NULL)
      return (0);
    else
      return (getSize(node->pLeft) + 1 + getSize(node->pRight));
  }

  // returns the length of the List
  int size() { return getSize(this); };

  // add right
  void addRight(const T rightNode) throw(const char*);
  void addRight(BinaryNode<T>* rightNode);

  // add left
  void addLeft(const T leftNode) throw(const char*);
  void addLeft(BinaryNode<T>* leftNode);

  T data;
  BinaryNode<T>* pLeft;
  BinaryNode<T>* pRight;
  BinaryNode<T>* pParent;
};

/*****************************************
 * deleteBinaryTree
 * recursively deletes the tree
 ****************************************/
template <class T>
void deleteBinaryTree(BinaryNode<T>* node) {
  if (node != NULL) {
    deleteBinaryTree(node->pLeft);
    deleteBinaryTree(node->pRight);
    delete node;
  }
}

/*****************************************
 * addLeft
 * Adds a BinaryNode to the left
 ****************************************/
template <class T>
void BinaryNode<T>::addLeft(BinaryNode<T>* leftNode) {
  pLeft = leftNode;
}

/*****************************************
 * addLeft
 * Adds a value to the left
 * (overloaded)
 ****************************************/
template <class T>
void BinaryNode<T>::addLeft(const T leftNode) throw(const char*) {
  if (!leftNode)
    return;

  try {
    BinaryNode<T>* pNew = new BinaryNode<T>(leftNode);
    pNew->pParent = this;
    pLeft = pNew;
  } catch (...) {
    throw "ERROR: Unable to allocate a node";
  }
}

/*****************************************
 * addRight
 * Adds a value to the right
 ****************************************/
template <class T>
void BinaryNode<T>::addRight(const T rightNode) throw(const char*) {
  if (!rightNode)
    return;

  try {
    BinaryNode<T>* pNew = new BinaryNode<T>(rightNode);
    pNew->pParent = this;
    pRight = pNew;
  } catch (...) {
    throw "ERROR: Unable to allocate a node";
  }
}

/*****************************************
 * addRight
 * Adds a BinaryNode to the right
 * (overloaded!)
 ****************************************/
template <class T>
void BinaryNode<T>::addRight(BinaryNode<T>* rightNode) {
  pRight = rightNode;
}

/*****************************************
 * OPERATOR <<
 * Displays bnode contents
 ****************************************/
template <class T>
ostream& operator<<(ostream& out, BinaryNode<T>* pStart) {
  if (pStart != NULL) {
    cout << pStart->pLeft;
    out << pStart->data << " ";
    cout << pStart->pRight;
  }

  return out;
}

#endif  // WEEK08_BNODE_H
