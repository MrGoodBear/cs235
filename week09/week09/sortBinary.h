/***********************************************************************
 * Module:
 *    Lesson 08, Sort Binary
 *    Brother JonesL, CS 235
 * Author:
 *    Cameron Lewis
 * Summary:
 *    This program implements a binary tree sort.
 ************************************************************************/

#ifndef SORT_BINARY_H
#define SORT_BINARY_H

#include "bst.h"

/*****************************************************
 * SORT BINARY
 * Perform the binary tree sort
 ****************************************************/
template <class T>
void sortBinary(T array[], int num) {
  BST<T>* sortTree = new BST<T>;
  for (int i = 0; i < num; i++)
    sortTree->insert(array[i]);

  int itIndex = 0;
  BSTIterator<T> it;
  for (it = sortTree->begin(); it != sortTree->end(); ++it) {
    array[itIndex] = it.operator*();
    itIndex++;
  }
}

#endif  // SORT_BINARY_H
