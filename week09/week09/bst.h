/***********************************************************************
 * Component:
 *    Week 09, Binary Search Tree (BST)
 *    Brother JonesL, CS 235
 * Author:
 *    Br. Helfrich
 *    Modified by Scott Ercanbrack - removed most of the the BST class
 *                                   functions, but left BST Iterator class
 *
 *
 * Summary:
 *    Creates a binary search tree
 ************************************************************************/

#ifndef BST_H
#define BST_H

#include "bnode.h"  // for BinaryNode
#include "stack.h"  // for Stack
#include <cassert>

// forward declaration for the BST iterator
template <class T>
class BSTIterator;

/*****************************************************************
 * CLASS :: BST
 * The official Binary Search Tree class
 *****************************************************************/
template <class T>
class BST {
 public:
  // constructor
  BST() : root(NULL){};

  // copy constructor
  BST(const BST<T>& rhs);

  ~BST();

  // iterators
  BSTIterator<T> begin();
  BSTIterator<T> end() { return BSTIterator<T>(NULL); }
  BSTIterator<T> rbegin();
  BSTIterator<T> rend() { return BSTIterator<T>(NULL); }

  // is the tree empty?
  bool empty() const { return (root == 0); }
  // removes an item
  void remove(BSTIterator<T>& it);

  // clears tree contents
  void clear() {
    deleteBinaryTree(root);
    root = NULL;
  }
  // inserts item
  void insert(const T& item) throw(const char*);

  // overloaded assignment operator
  BST<T>& operator=(const BST<T>& rhs) throw(const char*) {
    if (this != &rhs) {
      if (root != NULL)
        deleteBinaryTree(root);
      if (rhs.root == NULL)
        root = NULL;
      else
        copyTree(this->root, rhs.root);
    }
    return *this;
  }
  // returns the size of the tree
  int size() { return sizeAux(root); }

  // find a given item
  BSTIterator<T> find(const T& t);

 private:
  BinaryNode<T>* root;
  void copyTree(BinaryNode<T>*& copiedNode, BinaryNode<T>* node) {
    if (node == NULL)
      copiedNode = NULL;
    else {
      copiedNode = new BinaryNode<T>;
      copiedNode->data = node->data;
      copyTree(copiedNode->pLeft, node->pLeft);
      copyTree(copiedNode->pRight, node->pRight);
    }
  }

  // calculates the size of the tree
  int sizeAux(BinaryNode<T>* node) {
    if (node == NULL)
      return (0);
    else
      return (sizeAux(node->pLeft) + 1 + sizeAux(node->pRight));
  }
};

/*********************************************************
* copy constructor
**********************************************************/
template <class T>
BST<T>::BST(const BST<T>& rhs) {
  copyTree(this->root, rhs.root);
}

/*****************************************************
* Destructor
*******************************************************/
template <class T>
BST<T>::~BST() {
  deleteBinaryTree(root);
}

/*****************************************************
 * begin
 * Return the first node (left-most) in a binary search tree
 ****************************************************/
template <class T>
BSTIterator<T> BST<T>::begin() {
  Stack<BinaryNode<T>*> nodes;

  nodes.push(NULL);
  nodes.push(root);
  while (nodes.top() != NULL && nodes.top()->pLeft)
    nodes.push(nodes.top()->pLeft);

  return nodes;
}
/*************************************************
 * remove
 * Removes the node given by iterator
 ************************************************/
template <class T>
void BST<T>::remove(BSTIterator<T>& it) {
  BinaryNode<T>* x = it.getNode();
  BinaryNode<T>* parent = x->pParent;

  if (x->pLeft != 0 && x->pRight != 0) {
    BinaryNode<T>* xSucc = x->pRight;
    parent = x;

    while (xSucc->pLeft != 0) {
      parent = xSucc;
      xSucc = xSucc->pLeft;
    }
    x->data = xSucc->data;
    x = xSucc;
  }

  BinaryNode<T>* subTree = x->pLeft;
  if (subTree == 0)
    subTree = x->pRight;
  if (parent == 0)
    root = subTree;
  else if (parent->pLeft == x)
    parent->pLeft = subTree;
  else
    parent->pRight = subTree;

  delete x;
}

/*****************************************************
 * rbegin
 * Returns last node in binary search tree
 ****************************************************/
template <class T>
BSTIterator<T> BST<T>::rbegin() {
  Stack<BinaryNode<T>*> nodes;

  nodes.push(NULL);
  nodes.push(root);
  while (nodes.top() != NULL && nodes.top()->pRight)
    nodes.push(nodes.top()->pRight);

  return nodes;
}

/****************************************************
 * FIND
 * Returns a node
 ****************************************************/
template <class T>
BSTIterator<T> BST<T>::find(const T& t) {
  BinaryNode<T>* pCurrNode = root;
  bool dataFound = false;
  while (!dataFound && pCurrNode != NULL) {
    if (t < pCurrNode->data)
      pCurrNode = pCurrNode->pLeft;
    else if (pCurrNode->data < t)
      pCurrNode = pCurrNode->pRight;
    else {
      dataFound = true;
    }
  }
  if (dataFound)
    return pCurrNode;
  else
    return end();
}
/*****************************************************
 * insert
 * Inserts node at a tree location
 ****************************************************/
template <class T>
void BST<T>::insert(const T& item) throw(const char*) {
  try {
    BinaryNode<T>* locptr = root;
    BinaryNode<T>* parent = 0;

    while (locptr != NULL) {
      parent = locptr;
      if (item < locptr->data)
        locptr = locptr->pLeft;
      else
        locptr = locptr->pRight;
    }

    locptr = new BinaryNode<T>(item);
    if (parent == NULL)
      root = locptr;
    else if (item < parent->data) {
      parent->pLeft = locptr;
      locptr->pParent = parent;
    } else {
      parent->pRight = locptr;
      locptr->pParent = parent;
    }

  } catch (...) {
    throw "ERROR: Unable to allocate a node";
  }
}

/**********************************************************
 * BSTIterator
 * Forward and reverse iterator through binary search tree
 *********************************************************/
template <class T>
class BSTIterator {
 public:
  // constructors
  BSTIterator(BinaryNode<T>* p = NULL) { nodes.push(p); }
  BSTIterator(Stack<BinaryNode<T>*>& s) { nodes = s; }
  BSTIterator(const BSTIterator<T>& rhs) { nodes = rhs.nodes; }

  // assignment
  BSTIterator<T>& operator=(const BSTIterator<T>& rhs) {
    // need an assignment operator for the Stack class.
    nodes = rhs.nodes;
    return *this;
  }

  // compare
  bool operator==(const BSTIterator<T>& rhs) const {
    // only need to compare the leaf node
    return rhs.nodes.const_top() == nodes.const_top();
  }
  bool operator!=(const BSTIterator<T>& rhs) const {
    // only need to compare the leaf node
    return rhs.nodes.const_top() != nodes.const_top();
  }

  // de-reference. Cannot change because it will invalidate the BST
  T& operator*() { return nodes.top()->data; }

  // iterators
  BSTIterator<T>& operator++();
  BSTIterator<T> operator++(int postfix) {
    BSTIterator<T> itReturn = *this;
    ++(*this);
    return itReturn;
  }
  BSTIterator<T>& operator--();
  BSTIterator<T> operator--(int postfix) {
    BSTIterator<T> itReturn = *this;
    --(*this);
    return itReturn;
  }

  // must give friend status to remove so it can call getNode() from it
  friend void BST<T>::remove(BSTIterator<T>& it);
  // get the node pointer

 private:
  BinaryNode<T>* getNode() { return nodes.top(); }

  // the stack of nodes
  Stack<BinaryNode<T>*> nodes;
};

/**************************************************
 * BSTIterator
 * advance by one
 *************************************************/
template <class T>
BSTIterator<T>& BSTIterator<T>::operator--() {
  // do nothing if we have nothing
  if (nodes.top() == NULL)
    return *this;

  // if there is a left node, take it
  if (nodes.top()->pLeft != NULL) {
    nodes.push(nodes.top()->pLeft);

    // there might be more right-most children
    while (nodes.top()->pRight)
      nodes.push(nodes.top()->pRight);
    return *this;
  }

  // there are no left children, the right are done
  assert(nodes.top()->pLeft == NULL);
  BinaryNode<T>* pSave = nodes.top();
  nodes.pop();

  // if the parent is the NULL, we are done!
  if (NULL == nodes.top())
    return *this;

  // if we are the right-child, got to the parent.
  if (pSave == nodes.top()->pRight)
    return *this;

  // we are the left-child, go up as long as we are the left child!
  while (nodes.top() != NULL && pSave == nodes.top()->pLeft) {
    pSave = nodes.top();
    nodes.pop();
  }

  return *this;
}
/**************************************************
 * ITERATOR :: BSTIterator
 * advance by one
 *************************************************/
template <class T>
BSTIterator<T>& BSTIterator<T>::operator++() {
  // do nothing if we have nothing
  if (nodes.top() == NULL)
    return *this;

  // if there is a right node, take it
  if (nodes.top()->pRight != NULL) {
    nodes.push(nodes.top()->pRight);

    // there might be more left-most children
    while (nodes.top()->pLeft)
      nodes.push(nodes.top()->pLeft);
    return *this;
  }

  // there are no right children, the left are done
  assert(nodes.top()->pRight == NULL);
  BinaryNode<T>* pSave = nodes.top();
  nodes.pop();

  // if the parent is the NULL, we are done!
  if (NULL == nodes.top())
    return *this;

  // if we are the left-child, got to the parent.
  if (pSave == nodes.top()->pLeft)
    return *this;

  // we are the right-child, go up as long as we are the right child!
  while (nodes.top() != NULL && pSave == nodes.top()->pRight) {
    pSave = nodes.top();
    nodes.pop();
  }

  return *this;
}

#endif  // BST_H
